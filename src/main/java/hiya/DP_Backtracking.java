package hiya;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class DP_Backtracking {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str[] = br.readLine().split("[,\\s]");
        int[] a = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            a[i] = Integer.parseInt(str[i]);
        }
        boolean[] visited = new boolean[str.length];
        memory = new ArrayList<ArrayList<StringBuffer>>();
        for (int i = 0; i < str.length; i++) {
            memory.add(i, null);
        }
        output = findWays(a, 0, visited);
        HashSet<String> set = new HashSet<String>();
        for (StringBuffer buffer : output) {
            set.add(buffer.toString());
        }

        ArrayList<String> list = new ArrayList<String>();
        list.addAll(set);
        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                if (o1.length() > o2.length()) {
                    return 1;
                } else if (o1.length() < o2.length()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        if (list.isEmpty()) {
            System.out.println("failure");
        } else {
            System.out.println(list.get(0) + " out");
        }
    }

    static List<StringBuffer> output = new ArrayList<StringBuffer>();

    static ArrayList<ArrayList<StringBuffer>> memory;

    public static ArrayList<StringBuffer> findWays(int[] a, int index, boolean[] visited) {
        if (index >= a.length) {
            return new ArrayList<StringBuffer>(Arrays.asList(new StringBuffer()));
        }
        if (a[index] == 0) {
            return null;
        }
        List<List<StringBuffer>> lstBuffers = new ArrayList<List<StringBuffer>>();
        int val = a[index];
        ArrayList<StringBuffer> finalResultBuffer = new ArrayList<StringBuffer>();
        for (int i = 1; i <= val; i++) {
            ArrayList<StringBuffer> ret = new ArrayList<StringBuffer>();
            if (index + i < a.length && memory.get(index + i) != null) {
                ret = memory.get(index + i);
            } else {
                ret = findWays(a, index + i, visited);
                memory.add(index + i, ret);
            }

            if (ret != null && !ret.isEmpty()) {
                lstBuffers.add(ret);
            }
        }
        if (!lstBuffers.isEmpty()) {
            for (List<StringBuffer> lstBuff : lstBuffers) {
                for (StringBuffer buffer : lstBuff) {
                    finalResultBuffer.add(new StringBuffer(index + "").append(" ").append(buffer));
                }
            }
        }
        return finalResultBuffer;
    }
}
