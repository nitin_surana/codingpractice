package codejam2017;

import java.util.Scanner;

public class TidyNumbers {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int k = 1; k <=t; k++) {
            long l = sc.nextLong();
            for (long i = l; i >= 0; i--) {
                if (isTidy(i)) {
                    System.out.println("Case #" + k + ": " + i);
                    break;
                }
            }
        }
    }

    public static boolean isTidy(long l) {
        long lastDigit = l % 10;
        l = l / 10;
        while (l > 0) {
            long newDigit = l % 10;
            l = l / 10;
            if (newDigit <= lastDigit) {
                lastDigit = newDigit;
            } else {
                return false;
            }
        }
        if (l == 0) {
            return true;
        }
        return false;
    }
}
