package fidessa;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class p_1chk8is1k {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<Character, Integer>();
        for (char c : s.toCharArray()) {
            Integer i = map.get(c);
            if (i == null) {
                i = 1;
            } else {
                i++;
            }
            map.put(c, i);
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println(entry.getKey());
                break;
            }
        }
    }
}

//abcba
//aabba
//abba
//abccded
//abcCdedA