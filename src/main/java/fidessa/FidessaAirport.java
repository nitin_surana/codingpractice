package fidessa;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class FidessaAirport {
    /*      Cases Tested
6
900 490 950 1100 1500 1800
910 1200 1120 1130 1900 2000
Output : 3

2
900 1000
1000 1100
Output : 2
     */
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String str[] = br.readLine().split(" ");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(str[i]);
        }
        str = br.readLine().split(" ");
        int[] dep = new int[n];
        for (int i = 0; i < n; i++) {
            dep[i] = Integer.parseInt(str[i]);
        }
        Arrays.sort(dep);
        System.out.println(solution(arr, dep, n));
    }

    public static int solution(int arr[], int[] dep, int flights) {
        int maxLanded = 0;
        int landed = 0;
        int indexOne = 0;
        int indexTwo = 0;
        while (indexTwo < flights) {
            if (indexOne < flights && arr[indexOne] <= dep[indexTwo]) {
                indexOne++;
                landed++;
                if (landed > maxLanded) {
                    maxLanded = landed;
                }
            } else {
                landed--;
                indexTwo++;
            }
        }
        return maxLanded;
    }
}
