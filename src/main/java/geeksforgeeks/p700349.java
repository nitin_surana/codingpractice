package geeksforgeeks;

public class p700349{
    class Node {
        int data;
        Node left, right;

        Node(int d) {
            data = d;
            left = right = null;
        }
    }

    class GfG {
        boolean search(Node root, int x) {
            if (root.data == x) {
                return true;
            }
            if (x > root.data && root.right != null) {
                return search(root.right, x);
            } else if (x < root.data && root.left != null) {
                return search(root.left, x);
            }
            return false;
        }
    }
}