package geeksforgeeks;

public class p700311 {
    class Node {
        int data;
        Node left, right;

        Node(int item) {
            data = item;
            left = right = null;
        }
    }

    class GfG {

        boolean isHeap(Node tree) {
            int total_nodes = countNodes(tree);
            if (!isComplete(tree, 0, total_nodes)) {
                return false;
            }
            return checkHeap(tree);
        }

        boolean checkHeap(Node root) {
            if (root == null) {
                return true;
            }
            if (root.left != null && root.data < root.left.data) {
                return false;
            } else if (root.right != null && root.data < root.right.data) {
                return false;
            }
            return checkHeap(root.left) && checkHeap(root.right);
        }

        boolean isComplete(Node root, int index, int total_nodes) {
            if (root == null) {
                return true;
            }
            if (index >= total_nodes) {
                return false;
            }
            return isComplete(root.left, 2 * index + 1, total_nodes) && isComplete(root.right, 2 * index + 2, total_nodes);
        }

        int countNodes(Node root) {
            if (root == null) {
                return 0;
            }
            return 1 + countNodes(root.left) + countNodes(root.right);
        }
    }

    public static void main(String[] args) {
        p700311 p = new p700311();
        p.start();
    }

    public void start() {
        Node root = new Node(97);
        root.left = new Node(46);
        root.right = new Node(37);
        root.left.left = new Node(12);
        root.left.right = new Node(3);
        root.right.left = new Node(7);
        root.right.right = new Node(31);
        root.left.left.left = new Node(6);
        root.left.left.right = new Node(9);

        GfG g = new GfG();
//        boolean b = g.isComplete(root, 0, g.countNodes(root));
        boolean b = g.isHeap(root);
        System.out.println(b);

        root = new Node(97);
        root.left = new Node(46);
        root.right = new Node(37);
        root.left.left = new Node(12);
        root.left.right = new Node(3);
        root.right.left = new Node(7);
        root.right.right = new Node(31);
        root.left.right.left = new Node(2);
        root.left.right.right = new Node(4);

        g = new GfG();
//        b = g.isComplete(root, 0, g.countNodes(root));
        b=g.isHeap(root);
        System.out.println(b);
    }
}
