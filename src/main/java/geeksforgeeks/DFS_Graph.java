package geeksforgeeks;

import java.io.*;
import java.util.*;


public class DFS_Graph {

    class Graph {
        private int V;   // No. of vertices
        private LinkedList<Integer> adj[];

        Graph(int v) {
            V = v;
            adj = new LinkedList[v];
            for (int i = 0; i < v; ++i)
                adj[i] = new LinkedList();
        }

        void addEdge(int v, int w) {
            adj[v].add(w);
        }

        void DFS(int e) {
            Stack<Integer> stack = new Stack<Integer>();
            stack.push(e);
            boolean[] visited = new boolean[V];
            while (!stack.isEmpty()) {
                int a = stack.pop();
                if (visited[a]) {
                    continue;
                }
                visited[a] = true;
                System.out.println(a + " ");
                for (int n : adj[a]) {
                    if (!visited[n]) {
                        stack.push(n);
                    }
                }
            }
        }
    }

    public static void main(String args[]) {
        new DFS_Graph().start();
    }

    public void start() {
        Graph g = new Graph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(2, 0);
        g.addEdge(3, 3);

        System.out.println("Following is Depth First Traversal " +
                "(starting from vertex 2)");

        g.DFS(2);
    }
}
