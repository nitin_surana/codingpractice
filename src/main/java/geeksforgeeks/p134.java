package geeksforgeeks;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.Arrays;

public class p134 {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        for (int i = 0; i < t; i++) {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            String str[] = br.readLine().split(" ");
            for (int j = 0; j < n; j++) {
                a[j] = Integer.parseInt(str[j]);
            }
            //First Solution
//            System.out.println(lis(a));

            //Second solution
            int results[] = new int[n];
            lisRecursive(a, a.length - 1, results);
            Arrays.sort(results);
            System.out.println(results[results.length - 1]);
        }
    }

    // O(n*n)
    //    Length of longest increasing subsequence (may be non-contiguous)
//    public static int lis(int[] a) {
//        int result[] = new int[a.length];
//        result[0] = 1;
//        for (int i = 0; i < a.length; i++) {
//            int max = 0;
//            for (int j = 0; j < i; j++) {
//                if (a[i] > a[j]) {
//                    max = Math.max(result[j], max);
//                }
//            }
//            result[i] = max + 1;
//        }
////        for (int x : result) {
////            System.out.print(x + " ");
////        }
////        System.out.println();
//        Arrays.sort(result);
//        return result[a.length - 1];
//    }

    // O(n*n)
    public static int lisRecursive(int[] a, int index, int[] store) {        //top-down solve bigger and then recursively solve smaller problems!
        if (index <= 0) {
            return 1;
        }
        int max = 0;
        for (int i = 0; i < index; i++) {
            if (store[i] == 0) {
                store[i] = lisRecursive(a, i, store);
            }
            if (a[index] > a[i]) {
                max = Math.max(max, store[i]);
            }
        }
//        for (int x : store) {
//            System.out.print(x + "    ");
//        }
//        System.out.println();
        store[index] = max + 1;
        return max + 1;
    }

//    L(i) = 1 + Max(L(j))        0<j<i
}
