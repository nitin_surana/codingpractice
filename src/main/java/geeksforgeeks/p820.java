package geeksforgeeks;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class p820 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            String str[] = br.readLine().split(" ");
            int n = Integer.parseInt(str[0]);
            int m = Integer.parseInt(str[1]);
            int a[][] = new int[n][m];
            str = br.readLine().split(" ");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    a[i][j] = Integer.parseInt(str[i * m + j]);
                }
            }
            int ele = Integer.parseInt(br.readLine());
            boolean b = findElement(a, 0, n - 1, 0, m - 1, ele);
            if (b) {
                System.out.println(1);
            } else {
                System.out.println(0);
            }
        }
    }

    public static boolean findElement(int[][] a, int x1, int x2, int y1, int y2, int ele) {
        //Compare bottom-left element
        while (true) {
            if (x2 < 0 || y1 > y2) {
                return false;
            } else if (a[x2][y1] == ele) {
                return true;
            }
            if (ele > a[x2][y1]) {
                y1++;
            } else if (ele < a[x2][y1]) {
                x2--;
            }
        }
    }
}
