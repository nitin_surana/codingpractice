package geeksforgeeks;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class p164 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            String str[] = br.readLine().split(" ");
            int n = Integer.parseInt(str[0]);
            int m = Integer.parseInt(str[1]);
            int output[][] = new int[n + 1][m + 1];
            str = br.readLine().split(" ");
            char[] a = str[0].toCharArray();
            char[] b = str[1].toCharArray();
            int i = 0, j = 0;
            for (j = 1; j <= m; j++) {
                output[i][j] = j;
            }
            j = 0;
            for (i = 1; i <= n; i++) {
                output[i][j] = i;
            }

            for (i = 1; i <= n; i++) {
                for (j = 1; j <= m; j++) {
                    if (a[i - 1] == b[j - 1]) {
                        output[i][j] = output[i - 1][j - 1];
                    } else {
                        output[i][j] = Math.min(Math.min(output[i - 1][j - 1], output[i - 1][j]), output[i][j - 1]) + 1;
                    }
                }
            }
            System.out.println(output[n][m]);
        }
    }
}
