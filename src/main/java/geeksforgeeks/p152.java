package geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class p152 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            String[] str = br.readLine().split(" ");
            int n = Integer.parseInt(str[0]);
            int m = Integer.parseInt(str[1]);

            char[] narr = new char[n];
            char[] marr = new char[m];

            narr = br.readLine().toCharArray();
            marr = br.readLine().toCharArray();

            //First solution - recursive & memoization
//            int[][] results = new int[n][m];
//            for (int i = 0; i < n; i++) {
//                for (int j = 0; j < m; j++) {
//                    results[i][j] = -1;
//                }
//            }
//            int x = findLCS(narr, narr.length - 1, marr, marr.length - 1, results);
//            System.out.println(x);

            //Second solution - Iterative & dynamic programming & prints the LCS
            int r[][] = new int[narr.length + 1][marr.length + 1];
            int x = findLCS(narr, marr, r);
            System.out.println(x);
            StringBuffer strResult = new StringBuffer();
            //PrintLCS
            int i = n, j = m;
            while (i > 0 && j > 0) {
                if (r[i][j] == r[i - 1][j - 1] + 1) {
                    strResult.append(narr[i - 1]);
                    i--;
                    j--;
                } else if (r[i][j] == r[i - 1][j]) {
                    i--;
                } else {
                    j--;
                }
            }
            System.out.println(strResult.reverse());
        }
    }

    public static int findLCS(char[] a, char[] b, int[][] results) {
        int n = a.length;
        int m = b.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                if (i == 0 || j == 0) {
                    results[i][j] = 0;
                } else if (a[i - 1] == b[j - 1]) {
                    results[i][j] = 1 + results[i - 1][j - 1];
                } else {
                    results[i][j] = Math.max(results[i - 1][j], results[i][j - 1]);
                }
            }
        }
        return results[n][m];
    }

    //Recursive
    public static int findLCS(char[] a, int aindex, char[] b, int bindex, int[][] results) {
        if (aindex < 0 || bindex < 0) {
            return 0;
        }
        if (results[aindex][bindex] != -1) {
            return results[aindex][bindex];
        }
        if (a[aindex] == b[bindex]) {
            int s = 1 + findLCS(a, aindex - 1, b, bindex - 1, results);
            results[aindex][bindex] = s;
            return s;
        } else {
            int s = Math.max(findLCS(a, aindex - 1, b, bindex, results), findLCS(a, aindex, b, bindex - 1, results));
            results[aindex][bindex] = s;
            return s;
        }
    }
}
