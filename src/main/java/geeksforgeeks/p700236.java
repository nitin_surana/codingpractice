package geeksforgeeks;

public class p700236 {

    class Node {
        int data;
        Node left, right;

        Node(int item) {
            data = item;
            left = right = null;
        }
    }

    class GfG {
        Node lca(Node node, int n1, int n2) {
            if (node == null) {
                return null;
            }
            if (Math.max(n1, n2) < node.data) {
                return lca(node.left, n1, n2);
            } else if (Math.min(n1, n2) > node.data) {
                return lca(node.right, n1, n2);
            } else {
                return node;
            }
        }

    }
}