package geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Hashtable;

public class p166 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            int arr[] = new int[n];
            String[] str = br.readLine().split(" ");
            int totalsum = 0;
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(str[i]);
                totalsum += arr[i];
            }
            Arrays.sort(arr);

            boolean[][] matrix = new boolean[n][totalsum + 1];
            for (int j = 0; j < totalsum + 1; j++) {
                matrix[0][j] = arr[0] == j;
            }
            for (int i = 0; i < n; i++) {
                matrix[i][0] = true;
            }
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < totalsum + 1; j++) {
                    matrix[i][j] = matrix[i - 1][j] || (j - arr[i] > -1 ? matrix[i - 1][j - arr[i]] : false);
                }
            }

            int i = n - 1;
            int findSum = 1 + totalsum / 2;
            int diff = 0;
            int j = findSum - diff++;
            while (diff < findSum) {
                j = findSum - diff++;
                if (matrix[i][j]) {
                    break;
                }
            }
            System.out.println(totalsum - 2 * j);
        }
    }
}
