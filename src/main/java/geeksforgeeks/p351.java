package geeksforgeeks;

import java.nio.Buffer;
import java.util.*;
import java.lang.*;
import java.io.*;

public class p351 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            int arr[] = new int[n];
            String[] str = br.readLine().split(" ");
            int totalsum = 0;
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(str[i]);
                totalsum += arr[i];
            }
            Arrays.sort(arr);

            if (totalsum % 2 != 0) {        //if totalsum is odd, then 2 partitions with sum totalsum/2 can't be formed
                System.out.println("NO");
            } else {
                if (isSubsetSum(0, totalsum / 2, arr)) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            }
        }
    }

    static boolean isSubsetSum(int index, int sum, int[] a) {
        if (index > a.length - 1 || a[index] > sum) {
            return false;
        } else if (a[index] == sum) {
            return true;
        }
        return isSubsetSum(index + 1, sum - a[index], a) || isSubsetSum(index + 1, sum, a) ;
    }
}
