package geeksforgeeks;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class p700151 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            String[] str = br.readLine().split(" ");
            int a[] = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(str[i]);
            }
            quickSort(a, 0, a.length - 1);
            for (int x : a) {
                System.out.print(x + " ");
            }
            System.out.println();
        }
    }

    public static void quickSort(int[] a, int low, int high) {
        if (low < high) {
            int pi = partition(a, low, high);
            quickSort(a, low, pi - 1);
            quickSort(a, pi + 1, high);
        }
    }

    static int partition(int a[], int low, int high) {
        int pivot = high, start = low, end = high;
        while (low < high) {
            while (low <= end && a[low] <= a[pivot]) {
                low++;
            }
            while (high >= start && a[high] >= a[pivot]) {
                high--;
            }
            if (low < high) {
                int temp = a[low];
                a[low] = a[high];
                a[high] = temp;
            }
        }
        if (low <= end && low > high) {
            int temp = a[low];
            a[low] = a[pivot];
            a[pivot] = temp;
            return low;
        }
        return pivot;
    }
}
