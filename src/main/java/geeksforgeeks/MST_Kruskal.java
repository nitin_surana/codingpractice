package geeksforgeeks;

import java.util.*;
import java.lang.*;
import java.io.*;


public class MST_Kruskal {
    class Graph {

        class Edge implements Comparable<Edge> {
            int src, dest, weight;

            public int compareTo(Edge compareEdge) {
                return this.weight - compareEdge.weight;
            }
        }

        int V, E;    // V-> no. of vertices & E->no.of edges
        Edge edgesArray[]; // collection of all edges
        TreeSet<Edge> results = new TreeSet<Edge>();

        // Creates a graph with V vertices and E edges
        Graph(int v, int e) {
            V = v;
            E = e;
            edgesArray = new Edge[E];
            for (int i = 0; i < e; ++i) {
                edgesArray[i] = new Edge();
            }
        }

        void KruskalMST() {
            Arrays.sort(edgesArray);
            for (int i = 0; i < edgesArray.length; i++) {
                Edge e = edgesArray[i];
                if (!isCycle(e)) {
                    results.add(e);
                }
                if (results.size() == V - 1) {
                    break;
                }
            }
            for (Edge e : results) {
                System.out.println(e.weight + "  :   " + e.src + "    :   " + e.dest);
            }
        }

        boolean isCycle(Edge toInclude) {
            boolean visited[] = new boolean[V];
            for (Edge e : results) {
                visited[e.src] = true;
                visited[e.dest] = true;
            }
            if (visited[toInclude.src] && visited[toInclude.dest]) {
                return true;
            }
            return false;
        }
    }

    public void start() {

        /* Let us create following weighted graph
                 10
            0--------1
            |  \     |
           6|   5\   |15
            |      \ |
            2--------3
                4       */
        int V = 4;  // Number of vertices in graph
        int E = 5;  // Number of edges in graph
        Graph graph = new Graph(V, E);

        // add edge 0-1
        graph.edgesArray[0].src = 0;
        graph.edgesArray[0].dest = 1;
        graph.edgesArray[0].weight = 10;

        // add edge 0-2
        graph.edgesArray[1].src = 0;
        graph.edgesArray[1].dest = 2;
        graph.edgesArray[1].weight = 6;

        // add edge 0-3
        graph.edgesArray[2].src = 0;
        graph.edgesArray[2].dest = 3;
        graph.edgesArray[2].weight = 5;

        // add edge 1-3
        graph.edgesArray[3].src = 1;
        graph.edgesArray[3].dest = 3;
        graph.edgesArray[3].weight = 15;

        // add edge 2-3
        graph.edgesArray[4].src = 2;
        graph.edgesArray[4].dest = 3;
        graph.edgesArray[4].weight = 4;

        graph.KruskalMST();
    }

    public static void main(String[] args) {
        new MST_Kruskal().start();
    }
}
