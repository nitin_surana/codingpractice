package geeksforgeeks;

import java.lang.reflect.Array;
import java.util.*;
import java.lang.*;
import java.io.*;


public class Dijkstras {

    class ShortestPath {
        boolean visited[];
        Map<Integer, Integer> nodeDistance = new HashMap<Integer, Integer>();
        int parent[];

        void dijkstra(int[][] graph, int start) {
            int len = graph.length;
            visited = new boolean[len];
            parent = new int[len];
            LinkedList<Integer> queue = new LinkedList<Integer>();
            queue.add(start);
            parent[0] = -1;
            nodeDistance.put(0, 0);
            while (!queue.isEmpty()) {
                int pop = queue.remove();
                if (visited[pop]) {
                    queue.add(findNextNode());
                    continue;
                }
                visited[pop] = true;

                for (int i = 0; i < len; i++) {
                    if (graph[pop][i] != 0 && !visited[i]) {
                        if (nodeDistance.get(i) == null) {
                            nodeDistance.put(i, graph[pop][i] + nodeDistance.get(pop));
                            parent[i] = pop;
                        } else if (nodeDistance.get(i) > graph[pop][i] + nodeDistance.get(pop)) {
                            nodeDistance.put(i, graph[pop][i] + nodeDistance.get(pop));
                            parent[i] = pop;
                        }
                    }
                }
                int nextNode = findNextNode();
                if (nextNode > -1) {
                    queue.add(nextNode);
                }
            }
            printArray();
        }

        void printArray() {
            Set<Map.Entry<Integer, Integer>> entrySet = nodeDistance.entrySet();
            ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(entrySet);
            for (int i = 0; i < entrySet.size(); i++) {
                Map.Entry entry = list.get(i);
                System.out.println(entry.getKey() + " :   " + entry.getValue() + "    :   " + printParent(i));
            }
        }

        String printParent(int p) {
            if (parent[p] == -1) {
                return p + "";
            }
            return printParent(parent[p])+ " " + p;
        }

        int findNextNode() {
            int lowestKey = -1;
            int lowestValue = Integer.MAX_VALUE;
            for (Map.Entry<Integer, Integer> entry : nodeDistance.entrySet()) {
                if (entry.getValue() < lowestValue && !visited[entry.getKey()]) {
                    lowestKey = entry.getKey();
                    lowestValue = entry.getValue();
                }
            }
            return lowestKey;
        }

    }

    public static void main(String[] args) {
        new Dijkstras().start();
    }

    public void start() {
        int graph[][] = new int[][]{{0, 4, 0, 0, 0, 0, 0, 8, 0},
                {4, 0, 8, 0, 0, 0, 0, 11, 0},
                {0, 8, 0, 7, 0, 4, 0, 0, 2},
                {0, 0, 7, 0, 9, 14, 0, 0, 0},
                {0, 0, 0, 9, 0, 10, 0, 0, 0},
                {0, 0, 4, 14, 10, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 1, 6},
                {8, 11, 0, 0, 0, 0, 1, 0, 7},
                {0, 0, 2, 0, 0, 0, 6, 7, 0}
        };
        ShortestPath t = new ShortestPath();
        t.dijkstra(graph, 0);
    }
}
