package hackerearth.buyhatke;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class p99f78d66319a4fa49a40cbae0c48b920 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str[] = br.readLine().trim().split("\\s+");
        int n = Integer.parseInt(str[0]);
        int m = Integer.parseInt(str[1]);
        int x = Integer.parseInt(str[2]);
        boolean edges[][] = new boolean[n][n];
        boolean special[] = new boolean[n];
        str = br.readLine().trim().split("\\s+");       //Special cities
        for (String s : str) {
            int a = Integer.parseInt(s);
            special[a - 1] = true;
        }
        while (x-- > 0) {
            str = br.readLine().trim().split("\\s+");
            int a = Integer.parseInt(str[0]);
            int b = Integer.parseInt(str[1]);
            if (a != b) {
                edges[a - 1][b - 1] = true;
                edges[b - 1][a - 1] = true;
            }
        }
        long sum = 0;
        visited = new boolean[n];
        while (true) {
            int indexToTravel = -1;
            for (int i = 0; i < n; i++) {
                if (visited[i] == false) {
                    indexToTravel = i;
                    break;
                }
            }
            if (indexToTravel == -1) {
                break;
            }
            visitedCount = 0;
            long nn = BFSCountSpecial(indexToTravel, n, edges, special);
            sum += (nn * visitedCount);
        }
        System.out.println(sum);
    }

    static boolean visited[];
    static long visitedCount = 0;

    public static long BFSCountSpecial(int start, int n, boolean[][] edges, boolean[] special) {
        long specialCount = 0;
        LinkedList<Integer> queue = new LinkedList<Integer>();
        queue.add(start);
        while (!queue.isEmpty()) {
            int node = queue.remove();
            if (visited[node]) {
                continue;
            }
            visitedCount++;
            visited[node] = true;
            if (special[node]) {
                specialCount++;
            }
            for (int i = 0; i < n; i++) {
                if (edges[node][i] && !visited[i]) {
                    queue.add(i);
                }
            }
        }
        return specialCount;
    }
}
