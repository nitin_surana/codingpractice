package hackerearth.buyhatke;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class p6c03ef58943b44fe83bc04cb4bd5acbe {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str[] = br.readLine().trim().split(" ");
        int n = Integer.parseInt(str[0]);
        int m = Integer.parseInt(str[1]);
        int k = Integer.parseInt(str[2]);

        int[] rows = new int[m];
        str = br.readLine().trim().split(" ");
        int count = 0;
        boolean globalEmpty = true;
        for (String s : str) {
            int a = Integer.parseInt(s);
            a--;
            if (a < m && rows[a] < k) {
                rows[a]++;
            } else {
                int tempRows = m;
                boolean empty = false;
                while (globalEmpty && tempRows-- > 0) {
                    a++;
                    if (rows[a % m] < k) {
                        rows[a % m]++;
                        empty = true;
                        break;
                    }
                }
                if (!empty) {
                    globalEmpty = false;
                }
                count++;
            }
        }
        System.out.println(count);
    }
}
