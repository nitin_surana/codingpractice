package codility.liveramp;

import java.util.Arrays;

public class OrderByHeight {

    public int solution(int[] a) {
        int[] b = new int[a.length];
        System.arraycopy(a, 0, b, 0, a.length);
        Arrays.sort(b);

        int l = -1, r = a.length;

        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {     //first time when the left indexes do not match
                l = i;
                break;
            }
        }

        for (int i = a.length - 1; i >= 0; i--) {
            if (a[i] != b[i]) {
                r = i;
                break;
            }
        }
        if (l == -1) {      //already sorted
            return 0;
        } else {
            return r - l + 1;
        }
    }

//    public int solution(int[] A) {
//        //create another array, sort it
//        //find the first mis-match index from the start
//        //find the last mis-match index from the end
//        //return end-start
//        //long arrays are required, due to the element size.
//        //Looking at the complexity, looks like the solution I just described above should work fine. Let's start coding!
//        //Oh and yes, I have worked on codemirror earlier, so I know that you can actually play my response in a timely fashion :)
//
//        int B[] = new int[A.length];
//        System.arraycopy(A, 0, B, 0, A.length);
//        Arrays.sort(B);
//
//        int l = -1, r = A.length;
//
//        for (int i = 0; i < A.length; i++) {
//            if (A[i] != B[i]) {
//                l = i;
//                break;
//            }
//        }
//        for (int i = A.length - 1; i >= 0; i--) {
//            if (A[i] != B[i]) {
//                r = i;
//                break;
//            }
//        }
//        if (l == -1 && r == A.length) {   ////Already sorted
//            return 0;
//        }
//        return r - l + 1;
//        //time to test in IDE
//    }

    public static void main(String[] args) {
        OrderByHeight c = new OrderByHeight();
        int[] a = new int[]{1, 2, 34, 21, 3};
        int r = c.solution(a);
        System.out.println(r + "      " + (r == 3));

        a = new int[]{1, 2, 3, 4, 5};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 0));

        a = new int[]{5, 2, 3, 4, 1};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 5));

        a = new int[]{5, 4, 3, 2, 1};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 5));

        a = new int[]{1, 4, 3, 2, 5};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 3));

        a = new int[]{5};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 0));

        a = new int[]{5, 4, 6};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 2));

        a = new int[]{100000000, 100000000, 1000000006, 100000000};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 2));

        a = new int[]{1, 5, 4, 3, 2};
        r = c.solution(a);
        System.out.println(r + "      " + (r == 4));

    }
}
