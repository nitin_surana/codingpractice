package codility.liveramp;

public class Solution {

    public static void main(String[] args) {
        Solution sol = new Solution();
        String str = sol.solution(1, 8, 3, 2);
        System.out.println(str);
        System.out.println(str.equals(sol.solution(2, 3, 1, 8)));

        str = sol.solution(2, 4, 0, 0);
        System.out.println(str);
        System.out.println(str.equals(sol.solution(0, 0, 2, 4)));
        System.out.println(str.equals(sol.solution(4, 2, 0, 0)));

        str = sol.solution(3, 7, 0, 0);
        System.out.println(str);


        str = sol.solution(9, 9, 1, 7);
        System.out.println(str);
        System.out.println(str.equals(sol.solution(9, 9, 1, 7)));
        System.out.println(str.equals(sol.solution(7, 9, 1, 9)));


        str = sol.solution(9, 0, 2, 3);
        System.out.println(str);

        str = sol.solution(0, 0, 0, 0);
        System.out.println(str);

        str = sol.solution(2, 3, 5, 9);
        System.out.println(str);
    }

    public String solution(int A, int B, int C, int D) {
        //h1_h2:m1:m2
        //If any of them is 2, or 1 or 0 //None, then return NOT POSSIBLE
        //Keep track which one is used.
        int arr[] = new int[]{A, B, C, D};
        boolean used[] = new boolean[4];
        try {
            int h1 = arr[maxIndex(2, arr, used)];
            int h2 = -1;
            if (h1 > -1 && h1 == 2) {
                h2 = arr[maxIndex(3, arr, used)];
            } else {
                h2 = arr[maxIndex(9, arr, used)];
            }
            int m1 = arr[maxIndex(6, arr, used)];
            int m2 = arr[maxIndex(9, arr, used)];
//            if (h1 > -1 && h2 > -1 && m1 > -1 && m2 > -1) {
            return h1 + "" + h2 + ":" + m1 + "" + m2;
//            } else {
//                return "NOT POSSIBLE";
//            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return "NOT POSSIBLE";
        }
    }

    public int maxIndex(int limit, int[] arr, boolean used[]) {
        int index = -1;
        outer:
        while (limit >= 0) {       //pass for 2, pass for 1, pass for 0
            for (int i = 0; i < arr.length; i++) {
                int x = arr[i];
                if (!used[i] && x == limit) {
                    index = i;
                    used[i] = true;
                    break outer;
                }
            }
            limit--;
        }
        return index;
    }
}
