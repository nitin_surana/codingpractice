package codility.liveramp;

import java.util.ArrayList;
import java.util.List;

public class Solution2 {
    public static void main(String[] args) {
        Solution2 s = new Solution2();
        String str;
        str = s.solution(0, 0, 0, 0);      //00.00
        System.out.println(str);

        str = s.solution(2, 3, 5, 9);      //23.59
        System.out.println(str);

        str = s.solution(1, 5, 3, 9);      //19.53
        System.out.println(str);

        str = s.solution(9, 0, 0, 9);      //09:09
        System.out.println(str);

        str = s.solution(5, 0, 0, 9);      //00:59
        System.out.println(str);

        str = s.solution(9, 8, 7, 6);
        System.out.println(str);

        str = s.solution(0, 8, 7, 6);
        System.out.println(str);

        str = s.solution(0, 8, 7, 0);
        System.out.println(str);

        str = s.solution(1, 6, 5, 9);
        System.out.println(str);

        str = s.solution(0, 6, 5, 0);
        System.out.println(str);

        str = s.solution(0, 1, 5, 0);
        System.out.println(str);

        str = s.solution(0, 1, 2, 0);
        System.out.println(str);
        // solution(5,0,0,9);      //00:59
    }

    public String solution(int A, int B, int C, int D) {
        ArrayList<Integer> lst = new ArrayList<Integer>();
        lst.add(A);
        lst.add(B);
        lst.add(C);
        lst.add(D);
        StringBuffer result = new StringBuffer();

        int h1_index = -1, h2_index = -1, m1_index = -1, m2_index = -1, item = -1;     //h1h2:m1m2

        h1_index = getMax(lst, 2);       //0,1,2
        try {
            item = lst.remove(h1_index);
            result.append(item);

            if (item == 2) {
                h2_index = getMax(lst, 3);      //if 2, then 0..4
            } else if (item > -1) {
                h2_index = getMax(lst, 9);      //if 0 or 1, then 0..9
            }
            item = lst.remove(h2_index);
            result.append(item);
            result.append(":");

            m1_index = getMax(lst, 5);
            result.append(lst.remove(m1_index));
            m2_index = getMax(lst, 9);
            result.append(lst.remove(m2_index));

            if (h1_index > -1 && h2_index > -1 && m1_index > -1 && m2_index > -1) {
                return result.toString();
            } else {
                return "NOT POSSIBLE";
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return "NOT POSSIBLE";
        }
    }

    public int getMax(List<Integer> lst, int max) {
        int mm = -1;
        for (int i = 0; i < lst.size(); i++) {
            int item = lst.get(i);
            if (max >= item && (mm == -1 || item > lst.get(mm))) {
                mm = i;
            }
        }
        return mm;
    }
}
