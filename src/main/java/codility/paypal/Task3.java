package codility.paypal;

public class Task3 {
    public static void main(String[] args) {
        int t = -1000000000;
        System.out.println(t);
        t = 1000000000;
        System.out.println(t);
        int[] a = new int[]{0, 0, 0, 0, 1};
        System.out.println(solution(a) == 4);
        a = new int[]{1, 0, 0, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{1, 0, 0, 1, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{1, 1, 0, 1, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 0, 0, 1, 0, 0};
        System.out.println(solution(a) == 5);
        a = new int[]{1, 0, 1, 0, 1, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 1, 0, 1, 0, 1};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 0, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{0};
        System.out.println(solution(a) == 0);
        a = new int[]{0, 1};
        System.out.println(solution(a) == 1);
        a = new int[]{1, 0};
        System.out.println(solution(a) == 1);
        a = new int[]{1, 1, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{1, 1, 0, 1};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 1, 1, 1};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 1, 1};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 1, 1, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 1, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 0, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 0, 0, 1};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 0, 1, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 1, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{1, 0, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{1, 0, 0, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 1, 0, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 0, 1, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 0, 0, 1, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 0, 0, 0, 1};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 0, 1, 1, 1, 1, 1};
        System.out.println(solution(a) == 5);
        a = new int[]{1, 0, 0, 1, 1, 1, 1};
        System.out.println(solution(a) == 5);
        a = new int[]{1, 1, 0, 0, 1, 1, 1};
        System.out.println(solution(a) == 4);
        a = new int[]{1, 1, 1, 0, 0, 1, 1};
        System.out.println(solution(a) == 4);
        a = new int[]{1, 1, 1, 1, 0, 0, 1};
        System.out.println(solution(a) == 5);
        a = new int[]{1, 1, 1, 1, 1, 0, 0};
        System.out.println(solution(a) == 5);
        a = new int[]{1, 1, 1, 1, 0, 0, 0, 0};
        System.out.println(solution(a) == 6);
        a = new int[]{1, 1, 1, 0, 0, 0};
        System.out.println(solution(a) == 4);
        a = new int[]{1, 1, 0, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{1, 1, 0, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 1, 1, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 1, 1, 0, 1};
        System.out.println(solution(a) == 3);
        a = new int[]{0, 1, 1, 1, 1};
        System.out.println(solution(a) == 4);
        a = new int[]{0, 1, 0, 1, 0, 1};
        System.out.println(solution(a) == 2);
        a = new int[]{0, 0, 1, 1, 0, 0};
        System.out.println(solution(a) == 3);
        a = new int[]{1};
        System.out.println(solution(a) == 0);
        a = new int[]{1, 0};
        System.out.println(solution(a) == 1);
        a = new int[]{0, 0, 1};
        System.out.println(solution(a) == 2);
        a = new int[]{1, 0, 0};
        System.out.println(solution(a) == 2);
        a = new int[]{1, 0, 1};
        System.out.println(solution(a) == 2);
        for (int i = 0; i < 8; i++) {
            String str[] = String.format("%03d", i).split("");
            int[] arr = new int[str.length];
            for (int j = 0; j < str.length; j++) {
                arr[j] = Integer.parseInt(str[j]);
            }
            System.out.println(solution(arr) == 2);
        }
        for (int i = 0; i < 100; i++) {
            String binary = Integer.toBinaryString(i);//String.format("%04d", Integer.toBIinaryString(i));
            String str[] = binary.split("");
            int[] arr = new int[str.length];
            for (int j = 0; j < str.length; j++) {
                arr[j] = Integer.parseInt(str[j]);
            }
            System.out.println(binary + "     " + solution(arr));
//            System.out.println(solution(arr) == 2);
        }
        System.out.println(solution(new int[]{0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}));
    }

    static int solution(int[] A) {
        int n = A.length;
        int result = 0;
        for (int i = 0; i < n - 1; i++) {
            if (A[i] == A[i + 1])
                result = result + 1;
        }
        int r = 0;
        for (int i = 0; i < n; i++) {
            int count = 0;
            if (i > 0) {
                if (A[i - 1] != A[i])
                    count = count + 1;
                else
                    count = count - 1;
            }
            if (i < n - 1) {
                if (A[i + 1] != A[i])
                    count = count + 1;
                else
                    count = count - 1;
            }
            r = Math.max(r, count);
        }
        return result + r;
    }
}
