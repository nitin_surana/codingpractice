package codility.paypal;

public class Task1 {
    public static void main(String[] args) {
//        999,999,999
//        String str = Integer.toBinaryString(999999999);
//        System.out.println(str);
//        String find = Integer.toBinaryString(14);
//
//        System.out.println(str.indexOf(find));
//        System.out.println(Integer.parseInt("111011100", 2));

//        String main = Integer.toBinaryString(1953786);
//        System.out.println(main);
//        String find = Integer.toBinaryString(53);
//        System.out.println(find);
//        System.out.println(main.indexOf(find));
        System.out.println(solution(53, 1953786));
        System.out.println(solution(0, 123456627));
    }

    static public int solution(int A, int B) {
        return ("" + B).indexOf("" + A);
    }
}
