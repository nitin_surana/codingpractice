package amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class OverlappingWindows {

    public static void main(String[] args) {
        ArrayList<Integer> input = new ArrayList<Integer>(Arrays.asList(4, 2, 73, 11, -5));
        System.out.println(calculateWindowSums(input, 2));
        System.out.println(calculateWindowSums(input, 1));
        System.out.println(calculateWindowSums(input, 3));
        System.out.println(calculateWindowSums(input, 4));
        System.out.println(calculateWindowSums(input, 5));
        //Two
        System.out.println(calculateWindowSumsTwo(input, 2));
        System.out.println(calculateWindowSumsTwo(input, 1));
        System.out.println(calculateWindowSumsTwo(input, 3));
        System.out.println(calculateWindowSumsTwo(input, 4));
        System.out.println(calculateWindowSumsTwo(input, 5));
    }

    public static ArrayList<Integer> calculateWindowSums(ArrayList<Integer> list, int windowSize) {
        ArrayList<Integer> lst = new ArrayList<>();
        //Complexity is O(n*w)
        for (int i = 0; i < list.size() - windowSize + 1; i++) {
            int temp = 0;
            for (int j = 0; j < windowSize; j++) {
                temp += list.get(i + j);
            }
            lst.add(temp);
        }
        return lst;
    }

    public static ArrayList<Integer> calculateWindowSumsTwo(ArrayList<Integer> list, int windowSize) {
        //Complexity is O(n)
        ArrayList<Integer> sum = new ArrayList<>();
        sum.add(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            sum.add(sum.get(i - 1) + list.get(i));
        }
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < list.size() - windowSize + 1; i++) {
            int num = sum.get(i + windowSize - 1);
            if (i > 0) {
                num -= sum.get(i - 1);
            }
            ret.add(num);
        }
        return ret;
    }
}
