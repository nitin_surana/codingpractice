package amazon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class MinimizeCost {
    static class Connection {
        public String node1, node2;
        public int cost;

        public Connection(String node1, String node2, int cost) {
            this.node1 = node1;
            this.node2 = node2;
            this.cost = cost;
        }

        public String toString() {
            return this.node1 + " " + this.node2 + " " + this.cost;
        }
    }

    public static void main(String[] args) {
        ArrayList<Connection> lst = new ArrayList<>();
        Connection c1 = new Connection("a", "b", 1);
        lst.add(c1);
        Connection c2 = new Connection("a", "c", 5);
        lst.add(c2);
        Connection c3 = new Connection("b", "c", 4);
        lst.add(c3);
        System.out.println("Result : ");
        for (Connection c : minimumCostConnection(lst)) {
            System.out.println(c);
        }
    }

    public static ArrayList<Connection> minimumCostConnection(ArrayList<Connection> connections) {
        //Sort the edges/connections
        Collections.sort(connections, new Comparator<Connection>() {
            @Override
            public int compare(Connection o1, Connection o2) {
                return o1.cost - o2.cost;
            }
        });

        TreeSet<Subset> subsets = new TreeSet<>();
        for (Connection c : connections) {
            subsets.add(new Subset(c.node1, c.node1));
            subsets.add(new Subset(c.node2, c.node2));
        }

        ArrayList<Connection> result = new ArrayList<>();

        for (int i = 0; i < subsets.size(); i++) {
            Subset setOne = find(subsets, connections.get(i).node1);
            Subset setTwo = find(subsets, connections.get(i).node2);
            if (!setOne.rank.equals(setTwo.rank)) {
                Union(subsets, setOne, setTwo);
            }
        }

        // print the contents of result[] to display the built MST
        System.out.println("Following are the edges in the constructed MST");
        for (Connection c : result) {
            System.out.println(c.node1 + "       " + c.node2);
        }
        return result;
    }

    public static Subset find(TreeSet<Subset> subsets, String node) {
        Subset temp = null;
        for (Subset ss : subsets) {
            if (ss.vertex.equals(node)) {
                temp = ss;
                if (!ss.rank.equals(ss.vertex)) {
                    temp = find(subsets, ss.rank);
                }
            }
        }
        return temp;
    }

    public static void Union(TreeSet<Subset> subsets, Subset setOne, Subset setTwo) {
        Subset set1 = find(subsets, setOne.vertex);
        String rankOne = set1.rank;
        Subset set2 = find(subsets, setOne.vertex);
        String rankTwo = set2.rank;
        if (rankOne.compareTo(rankTwo) < 0) {
            set1.rank = set2.rank;
        } else if (rankOne.compareTo(rankTwo) > 0) {
            set2.rank = set1.rank;
        } else {
            set1.rank = set2.rank;
        }
    }

    static class Subset implements Comparable<Subset> {
        public String vertex;
        public String rank;

        public Subset(String vertex, String rank) {
            this.vertex = vertex;
            this.rank = rank;
        }

        @Override
        public int compareTo(Subset o) {
            return this.vertex.compareTo(o.vertex);
        }
    }
}
