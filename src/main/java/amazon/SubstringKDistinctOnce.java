package amazon;


import java.util.*;

public class SubstringKDistinctOnce {

    public static void main(String[] args) {
        System.out.println(subStringsLessKDist("awaglk", 4));
        System.out.println(subStringsLessKDist("awaglk", 0));
        System.out.println(subStringsLessKDist("awaglk", 6));
        System.out.println(subStringsLessKDist("awaglk", 3));
        System.out.println(subStringsLessKDist("awaglk", 2));
        System.out.println(subStringsLessKDist("awaglk", 1));

        System.out.println(subStringsLessKDist("awawglk", 4));
        System.out.println(subStringsLessKDist("abcdef", 4));

    }

    static public List<String> subStringsLessKDist(String inputString, int num) {
        Map<Character, Integer> mCharMap = new HashMap<>();
        List<String> mResultList = new ArrayList<String>();

        for (int i = 0; i + num <= inputString.length(); i++) {
            boolean isRepeated = false;
            for (int j = i; j < i + num; j++) {
                char c = inputString.charAt(j);
                if (mCharMap.containsKey(c)) {
                    if (!isRepeated) {
                        mCharMap.put(c, mCharMap.get(c) + 1);
                        isRepeated = true;
                    } else {
                        isRepeated = false;
                        break;
                    }
                } else {
                    mCharMap.put(c, 1);
                }
            }

            if (isRepeated) {
                mResultList.add(inputString.substring(i, i + num));
            }
            //empty the map
            mCharMap.clear();
        }

        return mResultList;
    }
}
