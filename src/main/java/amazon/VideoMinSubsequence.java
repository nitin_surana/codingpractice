package amazon;

import java.util.*;

public class VideoMinSubsequence {

    public static void main(String[] args) {
        VideoMinSubsequence v = new VideoMinSubsequence();
        List<Character> chars = Arrays.asList('a', 'b', 'c', 'a');
//        chars = Arrays.asList('a', 'b', 'a', 'b', 'c', 'b', 'a', 'c', 'a', 'd', 'e', 'f', 'e', 'g', 'd', 'e', 'h', 'i', 'j', 'h', 'k', 'l', 'i', 'j');
        chars = Arrays.asList('a', 'b', 'c');
        System.out.println(v.lengthEachScene(chars));
    }

    List<Integer> lengthEachScene(List<Character> inputList) {
        List<Integer> result = new ArrayList<>();

        Map<Character, Data> tempMap = new HashMap<>();
        ArrayList<Data> lstData = new ArrayList<>();

        for (int index = 0; index < inputList.size(); index++) {
            Character c = inputList.get(index);
            if (tempMap.get(c) != null) {
                tempMap.get(c).setEndIndex(index);
            } else {
                Data d = new Data();
                lstData.add(d);
                tempMap.put(c, d);

                d.setCh(c);
                d.setStartIndex(index);
                d.setEndIndex(index);
            }
        }

        System.out.println(lstData);

//        int endIndex = -1;
        int selEndIndex = -1;
        while (selEndIndex + 1 < inputList.size()) {
            System.out.println("\nStart Index " + selEndIndex);
            int i = 0;
            Data startCharacter = lstData.get(i);
            while (startCharacter.getStartIndex() < selEndIndex) {     //find the next character to start the subsequence
                startCharacter = lstData.get(++i);
            }
            selEndIndex = startCharacter.getEndIndex();

            System.out.println("Start Char " + startCharacter);

//            boolean found = false;
//        while (change) {
//            change = false;
            for (; i < lstData.size(); i++) {
                Data tempData = lstData.get(i);
                if (tempData.getStartIndex() < selEndIndex && tempData.getEndIndex() > selEndIndex) {
//                    if (selEndIndex < tempData.getEndIndex()) {
                    selEndIndex = tempData.getEndIndex();
//                    found = true;
//                    }
                }
            }
//        }
            System.out.println("End Index " + selEndIndex);
            int length = selEndIndex - startCharacter.getStartIndex() + 1;
//            if (!found) {
//                selEndIndex++;
//            }
            result.add(length);
        }
//        result.add(lstData.size() - selEndIndex);
        return result;
    }

    class Data {
        Character ch;
        int startIndex = -1;
        int endIndex = -1;

        public String toString() {
            return ch + " [" + startIndex + "  :   " + endIndex + "]";
        }

        public Character getCh() {
            return ch;
        }

        public void setCh(Character ch) {
            this.ch = ch;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public void setStartIndex(int startIndex) {
            this.startIndex = startIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        public void setEndIndex(int endIndex) {
            this.endIndex = endIndex;
        }
    }
}
