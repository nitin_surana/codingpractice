package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BoatTrip {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int c = sc.nextInt();
        int m = sc.nextInt();
        int max = c * m;
        boolean possible = true;
        for (int i = 0; i < n; i++) {
            int a = sc.nextInt();
            if (a > max) {
                possible = false;
            }
        }
        if (possible) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

    //Below is wrong, because there's no concept of carry-forward in the problem
//    public static void main(String[] args) throws Exception {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String[] str = br.readLine().split(" ");
//        int n = Integer.parseInt(str[0]);       //number of trips
//        int c = Integer.parseInt(str[1]);       //each boat capacity
//        int m = Integer.parseInt(str[2]);       //total boats
//        int[] a = new int[n];
//        str = br.readLine().split(" ");
//        for (int i = 0; i < n; i++) {
//            a[i] = Integer.parseInt(str[i]);
//        }
//        int carryForward = 0;
//        for (int i = 0; i < n; i++) {
//            int toCarry = a[i] + carryForward;
//            carryForward = 0;
//            int capacity = m * c;
//            if (capacity < toCarry) {
//                carryForward = toCarry - capacity;
//            }
//        }
//        if (carryForward == 0) {
//            System.out.println("Yes");
//        } else {
//            System.out.println("No");
//        }
//    }
}
