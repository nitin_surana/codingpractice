package hackerrank;

import java.util.Scanner;

public class TheGreatXor {
    //    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int q = in.nextInt();
//        for (int a0 = 0; a0 < q; a0++) {
//            long x = in.nextLong();
//            StringBuffer buffer = new StringBuffer();
//            int bLen = Long.toBinaryString(x).length() - 1;
//            while (bLen-- > 0) {
//                buffer.append("1");
//            }
//            long upto = 1;
//            if (buffer.length() > 0) {
//                upto = Long.parseLong(buffer.toString(), 2);
//            }
//            long count = 0;
//            for (long i = 1; i <= upto; i++) {
//                if ((i ^ x) > x) {
//                    count++;
//                }
//            }
//            System.out.println(count);
//        }
//    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int q = sc.nextInt();
        while (q-- > 0) {
            long x = sc.nextLong();
            StringBuffer buffer = new StringBuffer(Long.toBinaryString(x));
            int n = buffer.length();
            int index = buffer.length() - 1;
            long count = 0;
            while (index > 0) {
                if (buffer.charAt(index) == '0') {
                    count += Math.pow(2, n - index - 1);
                }
                index--;
            }
            System.out.println(count);
        }
    }
}
