package hackerrank.university_codesprint_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BreakingBestAndWorstRecords {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        int a[] = new int[n];
        String str[] = br.readLine().split(" ");
        for (int i = 0; i < n; i++) {
            a[i] = Integer.parseInt(str[i]);
        }
        int max = a[0];
        int min = a[0];
        int maxCount = 0;
        int minCount = 0;
        for (int i = 1; i < n; i++) {
            if (a[i] > max) {
                max = a[i];
                maxCount++;
            } else if (a[i] < min) {
                min = a[i];
                minCount++;
            }
        }
        System.out.println(maxCount + " " + minCount);
    }
}
