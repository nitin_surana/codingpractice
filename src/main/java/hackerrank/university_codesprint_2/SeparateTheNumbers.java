package hackerrank.university_codesprint_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SeparateTheNumbers {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int q = Integer.parseInt(br.readLine());
        while (q-- > 0) {
            String str = br.readLine();
            boolean bool = false;
            for (int i = 1; i <= str.length() / 2; i++) {
                if (isBeautiful(str, i)) {
                    System.out.println("YES " + str.substring(0, i));
                    bool = true;
                    break;
                }
            }
            if (!bool) {
                System.out.println("NO");
            }
        }
    }

    public static void f2(String[] args) {
        String str = "1234";
        System.out.println(str);
        System.out.println(isBeautiful(str, 1));
        System.out.println(isBeautiful(str, 2));

        str = "91011";
        System.out.println(str);
        System.out.println(isBeautiful(str, 1));
        System.out.println(isBeautiful(str, 2));
        System.out.println(isBeautiful(str, 3));

        str = "99100";
        System.out.println(str);
        System.out.println(isBeautiful(str, 1));
        System.out.println(isBeautiful(str, 2));
        System.out.println(isBeautiful(str, 3));

        for (String x : new String[]{"12345"}) {

        }
    }

    public static boolean isBeautiful(String s, int len) {
        int index = 0;
        while (index + 2 * len <= s.length()) {
            String one = s.substring(index, index + len);
            if (one.startsWith("0")) {
                return false;
            }
            String two = s.substring(index + len, index + 2 * len);
            if (two.startsWith("0")) {
                return false;
            }
            long iOne = Long.parseLong(one);
            long iTwo = Long.parseLong(two);
            if (iTwo - iOne == 1) {
                index += len;
            } else if (index + 2 * len < s.length()) {
                String three = s.substring(index + len, index + 2 * len + 1);
                long iThree = Long.parseLong(three);
                if (iThree - iOne == 1) {
                    index += len;
                    len++;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        if (index + len != s.length()) {
            return false;
        }
        return true;
    }
}
