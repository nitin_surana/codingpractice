package hackerrank.university_codesprint_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class TheStoryOfATree {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for (int a0 = 0; a0 < q; a0++) {
            int n = in.nextInt();
            boolean edges[][] = new boolean[n + 1][n + 1];
            for (int a1 = 1; a1 < n; a1++) {
                int u = in.nextInt();
                int v = in.nextInt();
                edges[u][v] = true;
                edges[v][u] = true;
            }
            int g = in.nextInt();
            int k = in.nextInt();
            int[][] parentArray = new int[k][2];
            for (int a1 = 0; a1 < g; a1++) {
                int u = in.nextInt();
                int v = in.nextInt();
                parentArray[a1][0] = u;
                parentArray[a1][1] = v;
            }

//            ArrayList<Integer> path = new ArrayList<Integer>();
//            HashMap<Integer, Integer> nodeIndexInPath = new HashMap<Integer, Integer>();

            //Find all nodes reachable by u (excluding edge u, v);
            int u = parentArray[0][0];      //parent
            int v = parentArray[0][1];      //child
            edges[u][v] = edges[v][u] = false;
            List<Integer> nodesReachableByU = doBFS(u, edges);

//            System.out.println("Nodes reachable by node : " + u);
//            System.out.println(nodesReachableByU);

            int winCount = 0;
            for (int i = 1; i < g; i++) {
                int parent = parentArray[i][0];
                int child = parentArray[i][1];
                int exists = nodesReachableByU.indexOf(parent);
                if (exists != -1) {
                    edges[parent][child] = edges[child][parent] = false;
                    List<Integer> nodesReachableByW = doBFS(parent, edges);

//                    System.out.println("Nodes reachable by node : " + parent);
//                    System.out.println(nodesReachableByW);

                    List<Integer> newReachableNodeList = new ArrayList<Integer>();
                    for (int x : nodesReachableByW) {
                        if (nodesReachableByU.contains(x)) {
                            newReachableNodeList.add(x);
                        }
                    }
                    nodesReachableByU = newReachableNodeList;
                } else {
                    //otherwise 2 cases
                    //either parent is a child of U
                    //or child is a child of U -  it's a failure!
                    //nodesReachableByU intersection nodesReachableByParent = nodesReachableByU
                }
            }
            System.out.println(nodesReachableByU.size() + "/" + n);
        }
    }

    //BFS tree rooted at u
    public static List<Integer> doBFS(int startNodeIndex, boolean edges[][]) {
        boolean visited[] = new boolean[edges.length];
        List<Integer> lst = new ArrayList<Integer>();
        LinkedList<Integer> queue = new LinkedList<Integer>();
        queue.add(startNodeIndex);
        while (!queue.isEmpty()) {
            int node = queue.remove();
            if (visited[node]) {
                continue;
            }
            lst.add(node);
            visited[node] = true;
            for (int i = 1; i < edges[0].length; i++) {
                if (edges[node][i] && visited[i] == false) {
                    queue.add(i);
                }
            }
        }
        return lst;
    }
}
