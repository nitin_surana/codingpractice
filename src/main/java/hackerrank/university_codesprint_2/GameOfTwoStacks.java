package hackerrank.university_codesprint_2;

import java.util.Scanner;

public class GameOfTwoStacks {

//    public static void main(String[] args) {
//        int x = 1000000000;
//        int a[] = new int[]{4, 6, 14, 20, 21};
//        System.out.println(floor(a, 0, a.length, 15) == 2);
//        System.out.println(floor(a, 0, a.length, 14) == 2);
//        System.out.println(floor(a, 0, a.length, 13) == 1);
//        System.out.println(floor(a, 0, a.length, 4) == 0);
//        System.out.println(floor(a, 0, a.length, 3) == -1);
//        System.out.println(floor(a, 0, a.length, 21) == 4);
//        System.out.println(floor(a, 0, a.length, 22) == 4);
//
//        a = new int[]{0, 0, 0, 0, 0};
//        System.out.println(floor(a, 0, a.length, 1) == 4);
//
//        a = new int[]{0, 1, 2, 3, 4, 5};
//        System.out.println(floor(a, 0, a.length, 4) == 4);
//
//        a = new int[]{0, 1, 1, 1, 1};
//        System.out.println(floor(a, 0, a.length, 1) == 4);
//        System.out.println(floor(a, 0, a.length, 0) == 0);
//        System.out.println(floor(a, 0, a.length, 10) == 4);
//        System.out.println(floor(a, 0, a.length, 2) == 4);
//
//        a = new int[]{2, 4, 6, 8, 10};
//        System.out.println(floor(a, 0, a.length, 1) == -1);
//        System.out.println(floor(a, 0, a.length, 2) == 0);
//        System.out.println(floor(a, 0, a.length, 3) == 0);
//        System.out.println(floor(a, 0, a.length, 4) == 1);
//        System.out.println(floor(a, 0, a.length, 5) == 1);
//        System.out.println(floor(a, 0, a.length, 9) == 3);
//        System.out.println(floor(a, 0, a.length, 10) == 4);
//        System.out.println(floor(a, 0, a.length, 11) == 4);
//        System.out.println(floor(a, 0, a.length, 113232) == 4);
//
//        a = new int[]{1, 1, 1, 2, 3, 4};
//        System.out.println(floor(a, 0, a.length, 1) == 2);
//        System.out.println(floor(a, 0, a.length, 2) == 3);
//
//        a = new int[]{1, 1, 2, 3, 4};
//        System.out.println(floor(a, 0, a.length, 1) == 1);
//        System.out.println(floor(a, 0, a.length, 2) == 2);
//
//        a = new int[]{1, 1, 1, 1, 4};
//        System.out.println(floor(a, 0, a.length, 1) == 3);
//        System.out.println(floor(a, 0, a.length, 2) == 3);
//
//        a = new int[]{1, 1, 1, 1, 1};
//        System.out.println(floor(a, 0, a.length, 1) == 4);
//        System.out.println(floor(a, 0, a.length, 2) == 4);
//
//        a = new int[]{1, 1, 1, 1, 1, 2, 6};
//        System.out.println(floor(a, 0, a.length, 1) == 4);
//        System.out.println(floor(a, 0, a.length, 2) == 5);
//
//        a = new int[]{1, 1, 1, 2, 2, 2};
//        System.out.println(floor(a, 0, a.length, 1) == 2);
//        System.out.println(floor(a, 0, a.length, 2) == 5);
//
//        a = new int[]{1, 1, 2, 2, 3, 3, 4, 4, 5, 5};
//        System.out.println(floor(a, 0, a.length, 0) == -1);
//        System.out.println(floor(a, 0, a.length, 1) == 1);
//        System.out.println(floor(a, 0, a.length, 2) == 3);
//        System.out.println(floor(a, 0, a.length, 3) == 5);
//        System.out.println(floor(a, 0, a.length, 4) == 7);
//        System.out.println(floor(a, 0, a.length, 5) == 9);
//        System.out.println(floor(a, 0, a.length, 6) == 9);
//        System.out.println(floor(a, 0, a.length, 66) == 9);
//
//
//        a = new int[]{1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5};
//        System.out.println(floor(a, 0, a.length, 0) == -1);
//        System.out.println(floor(a, 0, a.length, 1) == 2);
//        System.out.println(floor(a, 0, a.length, 2) == 5);
//        System.out.println(floor(a, 0, a.length, 3) == 8);
//        System.out.println(floor(a, 0, a.length, 4) == 11);
//        System.out.println(floor(a, 0, a.length, 5) == 14);
//        System.out.println(floor(a, 0, a.length, 6) == 14);
//        System.out.println(floor(a, 0, a.length, 66) == 14);
//    }


    public static int floor(int[] a, int start, int end, int item) {
        if (start >= end) {
            return -1;
        }
        int m = (start + end) / 2;
        if (start + 1 == end && a[m] <= item) {
            return m;
        }
        if (item >= a[m]) {
            return floor(a, m, end, item);
        } else {
            return floor(a, start, m, item);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int g = in.nextInt();
        for (int a0 = 0; a0 < g; a0++) {
            int n = in.nextInt();
            int m = in.nextInt();
            int x = in.nextInt();
            int[] a = new int[n];
            int max_a_pop = 0;
            int max_b_pop = 0;
            int sum_a[] = new int[n];
            int sum_b[] = new int[m];
            a[0] = in.nextInt();
            sum_a[0] = a[0];
            for (int a_i = 1; a_i < n; a_i++) {
                a[a_i] = in.nextInt();
                sum_a[a_i] = sum_a[a_i - 1] + a[a_i];
            }
            int[] b = new int[m];
            b[0] = in.nextInt();
            sum_b[0] = b[0];
            for (int b_i = 1; b_i < m; b_i++) {
                b[b_i] = in.nextInt();
                sum_b[b_i] = sum_b[b_i - 1] + b[b_i];
            }

            max_a_pop = floor(sum_a, 0, n, x);           //these are indexes
            max_b_pop = floor(sum_b, 0, m, x);

            int maxPops = Math.max(max_a_pop, max_b_pop);       //handle cases for ceil returning -1

            for (int i = 0; i < m; i++) {
                if (x - sum_b[i] < 0) {
                    break;
                } else {
                    int ceil = floor(sum_a, 0, n, x - sum_b[i]);
                    if (ceil != -1) {
                        if (sum_a[ceil] > x - sum_b[i]) {
                            ceil--;
                        }
                        if (ceil + i + 1 > maxPops) {
                            maxPops = ceil + i + 1;
                        }
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                if (x - sum_a[i] < 0) {
                    break;
                } else {
                    int ceil = floor(sum_b, 0, m, x - sum_a[i]);
                    if (ceil != -1) {
                        if (sum_b[ceil] > x - sum_a[i]) {
                            ceil--;
                        }
                        if (ceil + i + 1 > maxPops) {
                            maxPops = ceil + i + 1;
                        }
                    }
                }
            }
            System.out.println(maxPops + 1);
        }
    }
}
