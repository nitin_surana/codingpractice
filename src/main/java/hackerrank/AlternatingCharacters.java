package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AlternatingCharacters {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            char[] a = br.readLine().toCharArray();
            int count = 0;
            int n = a.length;
            for (int i = 0; i < n - 1; i++) {
                if (a[i] == a[i + 1]) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }
}
