package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class CutTheTreeTwo {

    static class Node {
        int data;
        long sum;
        int parent;
        int index;
        List<Integer> nodes = new ArrayList<Integer>();

        public Node(int index, int data) {
            this.data = data;
            this.index = index;
        }

        public void connectToNode(int v) {
            nodes.add(v);
        }

        public List<Integer> getAllNodes() {
            return nodes;
        }
    }

    static class Edge {
        int start;
        int end;

        Edge(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println(System.currentTimeMillis());
        long startTime = System.currentTimeMillis() / 1000;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] str = br.readLine().split(" ");
        Node a[] = new Node[n];
        for (int i = 0; i < n; i++) {
            a[i] = new Node(i, Integer.parseInt(str[i]));
        }

        List<Edge> edges = new ArrayList<Edge>();
        for (int i = 0; i < n - 1; i++) {
            str = br.readLine().split(" ");
            int u = Integer.parseInt(str[0]);
            int v = Integer.parseInt(str[1]);
            u--;
            v--;
            a[u].connectToNode(v);
            a[v].connectToNode(u);
            edges.add(new Edge(u, v));
        }

        Node rootNode = a[0];
        int[] visited = new int[n];
        visited[0] = 1;
        doDFS(0, a, visited);    //  a, 0, visited);

//        for (Node nn : a) {
//            System.out.println(nn.data + "  " + nn.sum);
//        }

        long totalSum = rootNode.sum;
        long min = Long.MAX_VALUE;
        for (Edge edge : edges) {
            long diff = 0;
            int x = edge.start;
            int y = edge.end;
            if (a[x].parent == y) {
                diff = Math.abs(totalSum - a[x].sum - a[x].sum);
            } else {
                diff = Math.abs(totalSum - a[y].sum - a[y].sum);
            }
            if (min > diff) {
                min = diff;
            }
        }
        System.out.println(min);
        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis() / 1000 - startTime);
    }

    //Iterative solution
    public static void doDFS(int index, Node[] arr, int[] visited) {
        Node start = arr[0];
        Stack<Node> stack = new Stack<Node>();
        stack.push(start);
        Set<Integer> lst = new HashSet<Integer>();
        System.out.println("Before DFS - " + System.currentTimeMillis());
        while (!stack.isEmpty()) {
            Node n = stack.pop();
            n.sum = n.data;

            List<Integer> nodes = n.getAllNodes();
            int count = 0;
            for (Integer ii : nodes) {
                if (visited[ii] != 1) {
                    visited[ii] = 1;
                    arr[ii].parent = n.index;
                    stack.push(arr[ii]);
                    count++;
                }
            }
            if (count == 0) {
                lst.add(n.index);
            }
        }
        System.out.println("After DFS - " + System.currentTimeMillis());

        visited = new int[arr.length];
//        for (Node nn : lst) {
//            visited.add(nn);
//        }
        System.out.println("Before somewhat BFS - " + System.currentTimeMillis());
        while (!lst.isEmpty()) {
            Set<Integer> nodeList = new HashSet<Integer>();
            for (int nodeN : lst) {
                Node n = arr[nodeN];
                if (n.parent != 0 && visited[n.parent] != 1) {
                    int tempVisited = 0;
                    long tempSum = 0;
                    int tempChildCount = 0;
                    for (int x : arr[n.parent].getAllNodes()) {
                        Node nn = arr[x];
                        if (nn.parent == n.parent) {
                            if (visited[nn.index] == 1) {
                                tempVisited++;
                            }
                            tempSum += nn.sum;
                            tempChildCount++;
                        }
                    }
                    if (tempVisited == tempChildCount) {
                        arr[n.parent].sum += tempSum;
                        nodeList.add(n.parent);
                        visited[n.parent] = 1;
                    }
                }
            }
            lst = nodeList;
        }
        System.out.println("After somewhat BFS - " + System.currentTimeMillis());
    }

//    public static long doDFS(Node start, List<Node> visited) {
//        List<Node> nodes = start.getAllNodes();
//        long sum = start.data;
//        for (int i = 0; i < nodes.size(); i++) {
//            Node nn = nodes.get(i);
//            if (!visited.contains(nn)) {
//                nn.parent = start;
//                visited.add(nn);
//                sum += doDFS(nn, visited);
//            }
//        }
//        start.sum = sum;
//        return sum;
//    }
}
