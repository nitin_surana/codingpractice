package hackerrank;

import java.util.Scanner;

public class LeonardoAndLuckyNumbers {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int q = sc.nextInt();
        while (q-- > 0) {
            long l = sc.nextLong();
            if (l % 7 == 0 || l % 4 == 0) {//|| l % 7 % 4 == 0 || l % 4 % 7 == 0) {
                System.out.println("Yes");
            } else {
//                System.out.println("No");
                boolean lucky = false;
                while (l > 0) {
                    l -= 7;
                    if (l > 3 && l % 4 == 0) {
                        System.out.println("Yes");
                        lucky = true;
                        break;
                    }
                }
                if (!lucky) {
                    System.out.println("No");
                }
            }
        }
    }
}
