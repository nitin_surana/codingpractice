package hackerrank.vgsa_fighton_code;

import java.lang.reflect.Array;
import java.util.*;

public class TheQuickestWayUp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            TreeMap<Integer, Integer> ladders = new TreeMap<Integer, Integer>();
            while (n-- > 0) {
                ladders.put(sc.nextInt(), sc.nextInt());
            }
            int m = sc.nextInt();
            TreeMap<Integer, Integer> snakes = new TreeMap<Integer, Integer>();
            while (m-- > 0) {
                snakes.put(sc.nextInt(), sc.nextInt());
            }
            int moves[] = new int[100 + 1];
            moves[1] = 0;
            for (int i = 2; i <= 7; i++) {
                moves[i] = 1;
                if (ladders.containsKey(i)) {     //there's a ladder starting at 2
                    moves[ladders.get(i)] = 1;
                }
            }
            System.out.println(countMoves(100, ladders, snakes, moves));
        }
    }

    static public int countMoves(int i, TreeMap<Integer, Integer> ladders, TreeMap<Integer, Integer> snakes, int[] moves) {
        if (i <= 1) {
            return 0;
        } else if (moves[i] != 0) {
            return moves[i];
        }
        ArrayList<Integer> countList = new ArrayList<Integer>();
        countList.add(1 + countMoves(i - 1, ladders, snakes, moves));
        countList.add(1 + countMoves(i - 2, ladders, snakes, moves));
        countList.add(1 + countMoves(i - 3, ladders, snakes, moves));
        countList.add(1 + countMoves(i - 4, ladders, snakes, moves));
        countList.add(1 + countMoves(i - 5, ladders, snakes, moves));
        countList.add(1 + countMoves(i - 6, ladders, snakes, moves));
        if (ladders.containsValue(i)) {       //if i is end of ladder
            countList.add(countMoves(findKeyByValue(ladders, i), ladders, snakes, moves));      //get number of ways to reach ladder bottom/start
        }
//        if (snakes.containsValue(i)) {       //if i is at the end of snakes
//            countList.add(countMoves(findKeyByValue(snakes, i), ladders, snakes, moves));       //get number of ways to reach snake head/start
//        }
        moves[i] = Collections.min(countList).intValue();
        if (ladders.containsKey(i)) {     //if i is at the start of ladder
            if (moves[ladders.get(i)] == 0) {
                moves[ladders.get(i)] = moves[i];      //set ladder end moves to be same as ladder start i
            } else {
                moves[ladders.get(i)] = Math.min(moves[ladders.get(i)], moves[i]);      //set ladder end moves to be same as ladder start i
            }
        }
        if (snakes.containsKey(i)) {      //if i is at the start of snake
            if (moves[snakes.get(i)] != 0) {
                moves[snakes.get(i)] = Math.min(moves[snakes.get(i)], moves[i]);        //set snake end to be the same as snake end
            } else {
                moves[snakes.get(i)] = moves[i];
            }
        }
        return moves[i];
    }

    static public int findKeyByValue(TreeMap<Integer, Integer> map, Integer value) {
        for (Integer i : map.keySet()) {
            if (map.get(i) == value) {
                return i;
            }
        }
        return -1;
    }
}
