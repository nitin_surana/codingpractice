package hackerrank.vgsa_fighton_code;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class FlippingBits {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            long l = sc.nextLong();
            char c[] = new char[32];
            for (int i = 0; i < 32; i++) {
                c[i] = '0';
            }
            char tempArr[] = new StringBuffer(Long.toBinaryString(l)).toString().toCharArray();
            for (int i = 0; i < tempArr.length; i++) {
                c[32 - tempArr.length + i] = tempArr[i];
            }
            StringBuffer str = new StringBuffer();
            for (int i = 0; i < 32; i++) {
                if (c[i] == '0') {
                    str.append("1");
                } else {
                    str.append("0");
                }
            }
            System.out.println(Long.parseLong(str.toString(), 2));
        }
    }
}
