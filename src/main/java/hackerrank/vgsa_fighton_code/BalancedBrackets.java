package hackerrank.vgsa_fighton_code;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Stack;

public class BalancedBrackets {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int q = Integer.parseInt(br.readLine());
        while (q-- > 0) {
            String str = br.readLine();//.trim();
            boolean match = isBalanced2(str);
            if (match) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }

    public static char reverse(char c) {
        if (c == '(') {
            return ')';
        } else if (c == '{') {
            return '}';
//        } else if (c == '[') {
        } else {
            return ']';
        }
    }

    public static boolean isBalanced2(String str) {
        char[] c = str.toCharArray();
        boolean match = true;
        //boolean push = true;
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == '(' || c[i] == '{' || c[i] == '[') {
                stack.push(c[i]);
            } else if (c[i] == ')' || c[i] == '}' || c[i] == ']') {
                if (stack.isEmpty()) {
                    match = false;
                    break;
                } else {
                    if (reverse(stack.pop().charValue()) != c[i]) {
                        match = false;
                        break;
                    }
                }
            }
        }
        if (!stack.isEmpty()) {
            return false;
        }
        return match;
    }
}
