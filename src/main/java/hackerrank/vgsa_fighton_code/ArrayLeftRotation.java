package hackerrank.vgsa_fighton_code;

import java.util.Scanner;

public class ArrayLeftRotation {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int d = sc.nextInt();
        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        reverse(a, 0, d - 1);
        reverse(a, d, n - 1);
        reverse(a, 0, n - 1);
        StringBuffer buffer = new StringBuffer();
        for (int x : a) {
            buffer.append(x + " ");
        }
        System.out.println(buffer.toString());
    }

    public static void reverse(int[] a, int start, int end) {
        for (int i = 0; i <= (end - start) / 2; i++) {
            int temp = a[start + i];
            a[start+i] = a[end - i];
            a[end - i] = temp;
        }
    }
}
