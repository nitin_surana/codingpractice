package hackerrank.zappos;

public class BermudaTriangle {
    public static void main(String[] args) {
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 3, 1, 0, 0) == 1);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 3, 1, 3, 1) == 3);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 0, 0, 0, 0) == 4);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 0, 0, 3, 1) == 2);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 3, 1, 7, 1) == 3);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 4, 2, 7, 1) == 3);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 4, 2, 7, 1) == 3);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 1, 4, 2, 7, 1) == 0);

        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 3, 5, 3, 2, 3, 1) == 3);
        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 3, 5, 3, 5, 4, 1) == 3);
        System.out.println(foundInBermudatriangle(-3, -1, -7, -1, -3, -5, -3, -5, -4, -1) == 3);

//        System.out.println(foundInBermudatriangle(3, 1, 7, 1, 5, 5, 4, 1, 4, 3) == 3);
//        System.out.println(foundInBermudatriangle(0, 0, 2, 0, 1, 1, 1, 0, 1, 0) == 3);
    }

    static int foundInBermudatriangle(int x1, int y1, int x2, int y2, int x3, int y3, int px, int py, int bx, int by) {
        if (!checkValidTriangle(x1, y1, x2, y2, x3, y3)) {
            return 0;
        }

        int A = area(x1, y1, x2, y2, x3, y3);
        int A1 = area(px, py, x2, y2, x3, y3);
        int A2 = area(x1, y1, px, py, x3, y3);
        int A3 = area(x1, y1, x2, y2, px, py);
        boolean pInside = (A == A1 + A2 + A3);

        int B1 = area(bx, by, x2, y2, x3, y3);
        int B2 = area(x1, y1, bx, by, x3, y3);
        int B3 = area(x1, y1, x2, y2, bx, by);
        boolean bInside = (A == B1 + B2 + B3);
        if (pInside && bInside) {
            return 3;
        } else if (pInside) {
            return 1;
        } else if (bInside) {
            return 2;
        } else {
            return 4;
        }
    }

    static boolean checkValidTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        int side1 = distance(x1, y1, x2, y2);
        int side2 = distance(x1, y1, x3, y3);
        int side3 = distance(x2, y2, x3, y3);
        return (side1 < side2 + side3) && (side2 < side1 + side3) && (side3 < side1 + side2);
    }

    static int area(int x1, int y1, int x2, int y2, int x3, int y3) {
        return (int) Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
    }

    static public int distance(int x1, int y1, int x2, int y2) {
        int d = (int) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
        return d;
    }
}
