package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.TreeMap;
import java.util.TreeSet;

public class MissingNumbers {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        String strArray[] = br.readLine().split(" ");
        TreeMap<Integer, Integer> mapFirst = new TreeMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            Integer input = Integer.parseInt(strArray[i]);
            if (mapFirst.get(input) != null) {
                mapFirst.put(input, mapFirst.get(input) + 1);
            } else {
                mapFirst.put(input, 1);
            }
        }

        int m = Integer.parseInt(br.readLine());
        strArray = br.readLine().split(" ");
        TreeMap<Integer, Integer> mapSecond = new TreeMap<Integer, Integer>();
        for (int i = 0; i < m; i++) {
            Integer inputSecond = Integer.parseInt(strArray[i]);
            if (mapSecond.get(inputSecond) != null) {
                mapSecond.put(inputSecond, mapSecond.get(inputSecond) + 1);
            } else {
                mapSecond.put(inputSecond, 1);
            }
        }

        TreeSet<Integer> resultSet = new TreeSet<Integer>();
        for (Integer x : mapSecond.keySet()) {
            if (mapFirst.get(x) == null) {
                resultSet.add(x);
            } else if (!mapSecond.get(x).equals(mapFirst.get(x))) {     //Stupid compared Integers using ==
                resultSet.add(x);
            }
        }

        String output = "";
        for (int x : resultSet) {
            output += (x + " ");
        }
        System.out.println(output.trim());
    }
}
