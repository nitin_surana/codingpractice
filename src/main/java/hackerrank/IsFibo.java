package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class IsFibo {

    public static void main(String[] args) throws Exception {
        long i = 0;
        long j = 1;
        ArrayList<Long> list = new ArrayList<Long>();
        list.add(new Long(i));
        while (j < 10000000000l) {
            long temp = j;
            j += i;
            i = temp;
            list.add(j);
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            long n = Long.parseLong(br.readLine());
            if (n == 0 || n == 1) {
                System.out.println("IsFibo");
            } else {
//                if (isPerfectSquare(n)) {
                if (list.contains(n)) {
                    System.out.println("IsFibo");
                } else {
                    System.out.println("IsNotFibo");
                }
            }
        }
    }

    public static boolean isPerfectSquare(long x) {
        long i = 5 * x * x;
        double d = Math.sqrt(i - 4);
        double e = Math.sqrt(i + 4);
        if (Math.floor(d) == Math.ceil(d)) {
            return true;
        } else if (Math.floor(e) == Math.ceil(e)) {
            return true;
        }
        return false;
    }
}
