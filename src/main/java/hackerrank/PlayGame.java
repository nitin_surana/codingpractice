package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PlayGame {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            String str[] = br.readLine().split(" ");
            int[] a = new int[n];
            long[] s = new long[n];
            long[] p = new long[n];
            long sum = 0;
            for (int i = n - 1; i >= 0; i--) {
                a[i] = Integer.parseInt(str[n - 1 - i]);

            }

            for (int i = 0; i < n; i++) {
                sum += (long) a[i];
                s[i] = sum;
            }

            if (n <= 3) {
                System.out.println(sum);
            } else {
                p[0] = s[0];
                p[1] = s[1];
                p[2] = s[2];
                for (int i = 3; i < n; i++) {
                    long v1 = s[i - 1] - p[i - 1] + a[i];
                    long v2 = s[i - 2] - p[i - 2] + a[i] + a[i - 1];
                    long v3 = s[i - 3] - p[i - 3] + a[i] + a[i - 1] + a[i - 2];
                    p[i] = Math.max(Math.max(v1, v2), v3);
                }
                System.out.println(p[n - 1]);
            }
        }
    }
}