package hackerrank;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SherlockAndPairs {
    public static void main(String[] args) throws Exception {
//        System.setIn(new FileInputStream("sherlock_and_pairs.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            String[] str = br.readLine().split(" ");
            int a[] = new int[n];
            Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(str[i]);
                List<Integer> lst = map.get(a[i]);
                if (lst == null) {
                    lst = new ArrayList<Integer>();
                }
                lst.add(i);
                map.put(a[i], lst);
            }
            long sum = 0;
            for (Integer key : map.keySet()) {
                int size = map.get(key).size();
                if (size > 1) {
                    sum += permute(size);
                }
            }
            System.out.println(sum);
        }
    }

    //nP2 = n! / c
    public static long permute(int size) {
        return (long) size * (size - 1);
    }
}
