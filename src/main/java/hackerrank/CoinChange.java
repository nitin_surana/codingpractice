package hackerrank;

import java.util.Scanner;

public class CoinChange {

    //    https://www.hackerrank.com/challenges/coin-change
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] a = new int[m];
        for (int i = 0; i < m; i++) {
            a[i] = sc.nextInt();
        }
        long arr[][] = new long[m][n + 1];
        for (int i = 0; i < m; i++) {
            arr[i][0] = 1;
        }
        System.out.println(findCoinsToMake(n, m - 1, a, arr));
    }

    public static long findCoinsToMake(int sum, int k, int[] coins, long[][] arr) {
        if (sum > 0 && k < 0) {
            return 0;
        } else if (arr[k][sum] != 0) {
            return arr[k][sum];
        }
        long x = 0;
        if (coins[k] <= sum) {
            x = findCoinsToMake(sum - coins[k], k, coins, arr);
        }
        arr[k][sum] = x + findCoinsToMake(sum, k - 1, coins, arr);
        return arr[k][sum];
    }

//    public static int findCoinsToMake(int sum, int k, int[] coins) {
//        if (sum == 0) {     //Only 1 way to make a sum of zero
//            return 1;
//        } else if (sum > 0 && k < 0) {
//            return 0;
//        }
//        int x = 0;
//        if (coins[k] <= sum) {
//            x = findCoinsToMake(sum - coins[k], k, coins);
//        }
//        return x + findCoinsToMake(sum, k - 1, coins);
//    }
}
