package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HalloweenParty {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            long k = Long.parseLong(br.readLine());
            long s = (long) Math.pow(k / 2, 2);
            if (k % 2 == 0) {       //even
                System.out.println(s);
            } else {
                System.out.println(s + k / 2);
            }
        }
    }
}
