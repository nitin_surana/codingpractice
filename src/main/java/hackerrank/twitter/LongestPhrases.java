package hackerrank.twitter;

public class LongestPhrases {

    public static void main(String[] args) {
        System.out.println(maxLength(new int[]{1, 2, 3}, 2));
        System.out.println(maxLength(new int[]{1, 2, 3}, 4));
    }

    static int maxLength(int[] a, int k) {
        int result = 0;
        int maxSum = 0;
        for (int i = 0; i < a.length; i++) {
            int sum = 0;
            for (int j = i; j < a.length; j++) {
                sum += a[j];
                if (sum > maxSum && sum <= k) {
                    maxSum = sum;
                    result = j - i + 1;
                } else if (sum > k) {
                    break;
                }
            }
        }
        return result;
    }
}
