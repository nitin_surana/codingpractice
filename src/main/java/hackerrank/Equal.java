package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Equal {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            int a[] = new int[n];
            String str[] = br.readLine().split(" ");
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(str[i]);
                if (a[i] < min) {
                    min = a[i];
                }
            }
            int min1 = countOperations(a, min);
            int min2 = countOperations(a, min - 1);
            int min3 = countOperations(a, min - 2);
            int min4 = countOperations(a, min - 3);
            int min5 = countOperations(a, min - 4);
            int min6 = countOperations(a, min - 5);
            int m = Math.min(Math.min(min1, min2), Math.min(min3, min4));
            m = Math.min(m, Math.min(min5, min6));
            System.out.println(m);
        }
    }

    //Note that increasing everyone but i, is the same as decreasing only i
    //Allowed operations are -5, -2, -1
    public static int countOperations(int[] a, int min) {
        int count = Integer.MAX_VALUE;
        int totalOps = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == min) {
                continue;
            }
            int x = a[i] - min;
            int ops = x / 5 + (x % 5) / 2 + (x % 5) % 2;
            totalOps += ops;
        }
        return totalOps;
    }
}
