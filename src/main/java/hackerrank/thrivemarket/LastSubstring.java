package hackerrank.thrivemarket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class LastSubstring {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        char[] arr = str.toCharArray();
        Arrays.sort(arr);
        char maxChar = arr[arr.length - 1];     //find the max char
        TreeSet<String> set = new TreeSet<>();
        int index = -1;
        while (str.indexOf(maxChar, index + 1) != -1) {
            index = str.indexOf(maxChar, index + 1);
//            int lastIndex = str.length() - 1;
            set.add(str.substring(index));
        }
        System.out.println(set.last());
    }
}
