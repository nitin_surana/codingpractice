package hackerrank.thrivemarket;

import java.util.*;

public class CanYouSort {
    public static void main(String[] args) {
        CanYouSort c = new CanYouSort();
//        c.customSort(new int[]{3, 1, 2, 2, 4});
//        c.customSort(new int[]{8, 5, 5, 5, 5, 1, 1, 1, 4, 4});
//        c.customSort(new int[]{1, 2, 3, 4, 5});
//        c.customSort(new int[]{1, 2, 3, 4, 51, 2, 3});
        c.customSort(new int[]{61, 2, 3, 4, 51, 2, 3});
    }

    public void customSort(int[] arr) {
        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        for (int x : arr) {
            if (map.get(x) != null) {
                map.put(x, map.get(x) + 1);
            } else {
                map.put(x, 1);
            }
        }
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int nn = map.get(o1).compareTo(map.get(o2));
                if (nn == 0) {      //freq are equal
                    nn = o1.compareTo(o2);
                }
                return nn;
            }
        };
        TreeMap<Integer, Integer> treeMap = new TreeMap<>(comparator);
        treeMap.putAll(map);
        for (Map.Entry<Integer, Integer> entry : treeMap.entrySet()) {
            for (int i = 1; i <= entry.getValue(); i++) {
                System.out.println(entry.getKey());
            }
        }
    }
}
