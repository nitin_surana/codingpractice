package hackerrank.thrivemarket;

import java.util.LinkedHashMap;
import java.util.Map;

public class FindFirstRepetition {
    public static void main(String[] args) {
        FindFirstRepetition f = new FindFirstRepetition();
        System.out.println(f.firstRepeatingLetter("abcba") == 'a');
        System.out.println(f.firstRepeatingLetter("abcbb") == 'b');
        System.out.println(f.firstRepeatingLetter("nitin") == 'n');
        System.out.println(f.firstRepeatingLetter("niti") == 'i');
    }

    public char firstRepeatingLetter(String s) {
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();      //preserves insertion order
        for (char c : s.toCharArray()) {
            if (map.get(c) != null) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() > 1) {
                return entry.getKey();
            }
        }
        return '0';     //what to do if nothing repeats!
    }
}
