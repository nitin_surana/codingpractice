package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindTheMedian {

    public static void main(String[] args) throws Exception {
        Pattern pattern = Pattern.compile("\\w+(\\d+)");
        Matcher matcher = pattern.matcher("south1");

        if (matcher.find()) {
            System.out.println(matcher.group(1));
        }

        TreeSet<Integer> set = new TreeSet<Integer>();
        callMe(set.toArray(new Integer[]{}));


//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        int n = Integer.parseInt(br.readLine());
//        String[] str = br.readLine().split(" ");
//        int a[] = new int[n];
//        for (int i = 0; i < n; i++) {
//            a[i] = Integer.parseInt(str[i]);
//        }
//        findMedian(a, 0, a.length - 1);
//        int mIndex = a.length / 2;
//        System.out.println(a[mIndex]);
    }

    public static void callMe(Integer[] abc) {

    }


    static void findMedian(int[] a, int start, int end) {
        int mIndex = a.length / 2;
        int pi = partition(a, 0, end);
        while (pi != mIndex) {
            if (pi < mIndex) {
                pi = partition(a, pi + 1, end);
            } else if (pi > mIndex) {
                pi = partition(a, start, pi - 1);
            }
        }
    }

    static int partition(int a[], int low, int high) {
        int pivot = high, start = low, end = high;
        while (low < high) {
            while (low <= end && a[low] <= a[pivot]) {
                low++;
            }
            while (high >= start && a[high] >= a[pivot]) {
                high--;
            }
            if (low < high) {
                int temp = a[low];
                a[low] = a[high];
                a[high] = temp;
            }
        }
        if (low <= end && low > high) {
            int temp = a[low];
            a[low] = a[pivot];
            a[pivot] = temp;
            return low;
        }
        return pivot;
    }
}
