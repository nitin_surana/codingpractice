package hackerrank;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

public class ClosestNumbers {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] str = br.readLine().split(" ");
        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = Integer.parseInt(str[i]);
        }
        Arrays.sort(a);
        int min = Integer.MAX_VALUE;
        List<Pair> result = new ArrayList<Pair>();
        for (int i = 0; i < a.length - 1; i++) {
            int diff = Math.abs(a[i] - a[i + 1]);
            int x = Math.max(a[i], a[i + 1]);
            int y = Math.min(a[i], a[i + 1]);
            if (min == diff) {
                result.add(new Pair(y, x));
            } else if (diff < min) {
                min = diff;
                result.clear();
                result.add(new Pair(y, x));
            }
        }
        Collections.sort(result, new Pair(0, 0));
        for (Pair p : result) {
            System.out.print(p.a + " " + p.b + " ");
        }
        System.out.println();
    }

    static class Pair implements Comparator<Pair> {
        int a, b;

        Pair(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public int compare(Pair o1, Pair o2) {
            return o1.a - o2.a;
        }
    }
}
