package hackerrank.medallia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class WordRelatedness {

    static Map<String, TreeSet<String>> wordMap = new HashMap<>();

    /**
     * Parse the data into structures that can then be used by the findMean and findMedian functions
     * You MAY change the method signature as needed!
     */
    public static void parse(List<List<String>> dataSet) {
        for (List<String> line : dataSet) {
            for (String word : line) {
                for (String neighbour : line) {
                    if (wordMap.get(word) == null) {
                        wordMap.put(word, new TreeSet<>());
                    }
                    if (!neighbour.equals(word)) {
                        wordMap.get(word).add(neighbour);
                    }
                }
            }
        }
    }

    /**
     * @return mean of the degree of the words
     * Please DO NOT change the method signature
     */
    public static double findMean(List<List<String>> dataSet) {
        parse(dataSet);
        double count = wordMap.size();
        int sum = 0;
        for (Set set : wordMap.values()) {
            sum += set.size();
        }
        return sum / count;
    }

    /**
     * @return median of the degree of the words
     * Please DO NOT change the method signature
     */
    public static double findMedian(List<List<String>> dataSet) {
        parse(dataSet);
        TreeSet<Integer> st = new TreeSet<>();
        for (Set set : wordMap.values()) {
            st.add(set.size());
        }
        ArrayList<Integer> arrayList = new ArrayList<>(st);
        if (st.size() % 2 == 0) {       //even
            return (arrayList.get(st.size() / 2) + arrayList.get(st.size() / 2 - 1)) / 2.0;
        } else {        //odd
            return arrayList.get(st.size() / 2);
        }

    }


    /**
     * The following code is not relevant for this exercise. It is only here to help
     * run tests.
     */
    public static void main(String[] args) throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        List<List<String>> dataSet = new ArrayList<>();
        String line;
        while ((line = buffer.readLine()) != null) {
            if (line.trim().length() == 0) {
                break;
            }
            List<String> data = Arrays.asList(line.split(" ")).stream()
                    .map(word -> word.toLowerCase())
                    .collect(Collectors.toList());
            dataSet.add(data);
        }

        System.out.format("%s %s", new DecimalFormat("0.00").format(findMean(dataSet)), new DecimalFormat("0.00").format(findMedian(dataSet)));
    }
}

