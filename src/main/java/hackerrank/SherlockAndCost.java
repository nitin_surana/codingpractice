package hackerrank;

import java.io.*;

public class SherlockAndCost {

    public static void main(String[] args) throws Exception {
//        System.setIn(new FileInputStream(new File("input.txt")));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine());
            int a[] = new int[n];
            String str[] = br.readLine().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(str[i]);
            }
            System.out.println(count(a, n));
        }
    }

    public static int count(int[] a, int n) {
        int r[][] = new int[n][2];
        r[0][0] = r[0][1] = 0;
        for (int i = 1; i < a.length; i++) {
            int m1 = r[i - 1][0] + Math.abs(1 - 1);
            int m2 = r[i - 1][1] + Math.abs(1 - a[i - 1]);
            r[i][0] = Math.max(m1, m2);

            m1 = r[i - 1][0] + Math.abs(a[i] - 1);
            m2 = r[i - 1][1] + Math.abs(a[i] - a[i - 1]);
            r[i][1] = Math.max(m1, m2);
        }
        return Math.max(r[n - 1][0], r[n - 1][1]);
    }
}