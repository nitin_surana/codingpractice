package hackerrank;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class CountLuck {

    static class Point {
        public int x, y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            Point p = (Point) o;
            return (p.x == this.x && p.y == this.y);
        }
    }

    static class Result {
        public Stack<Point> stack;
        public int count;

        Result(Stack<Point> s, int c) {
            this.stack = s;
            this.count = c;
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            finalPath = new Stack<Point>();
//            finalCount = 0;

            Point start = null, end = null;
            String s = br.readLine();
            int n = Integer.parseInt(s.split(" ")[0]);
            int m = Integer.parseInt(s.split(" ")[1]);
            int[][] a = new int[n][m];
            for (int i = 0; i < n; i++) {
                String str = br.readLine();
                for (int j = 0; j < m; j++) {
                    if (str.charAt(j) == 'X') {
                        a[i][j] = -1;
                    } else if (str.charAt(j) == '.') {
                        a[i][j] = 0;
                    } else if (str.charAt(j) == 'M') {
                        a[i][j] = 0;
                        start = new Point(i, j);
                    } else if (str.charAt(j) == '*') {
                        a[i][j] = 0;
                        end = new Point(i, j);
                    }
                }
            }
            int k = Integer.parseInt(br.readLine());
//            Result result = findPath(a, start, end);
            int[][] visited = new int[a.length][a[0].length];
            findPath(a, start, end, visited);
//            traversePath(a, start, end, visited);
//            Stack<Point> stack = result.stack;
            Stack<Point> stack = finalPath;
            ArrayList<Point> myArrayList = new ArrayList<Point>(stack);
            Collections.reverse(stack);
            int myCount = 0;
//            int index = 0;
            while (!stack.isEmpty()) {
                Point temp = stack.pop();
                if (temp.equals(end)) {
                    continue;
                }
//                System.out.println(temp.x + "    " + temp.y);
//                if (findWays(a, temp).size() > 2) {
//                    myCount++;
//                }
                List<Point> ll = findWays(a, temp);
                int cc = 0;
                for (Point pp : ll) {
                    if (!myArrayList.contains(pp)) {
                        cc++;
                    }
                }
                if (cc > 0) {
                    myCount++;
                }
            }
//            System.out.println(myCount == k);
//            System.out.println(myCount);
//            System.out.println(result.count);
//            if (result.count == k) {
            if (myCount == k) {
                System.out.println("Impressed");
            } else {
                System.out.println("Oops!");
            }
//            System.out.println();
        }
    }

//    public static Result findPath(int[][] a, Point start, Point end) {
//        int[][] visited = new int[a.length][a[0].length];
//        int count = 0;
//        Stack<Point> path = new Stack<Point>();
//        Stack<Point> stack = new Stack<Point>();
//        stack.push(start);
//        while (!stack.isEmpty()) {
//            Point p = stack.pop();
//            if (isVisited(a, visited, p)) {
//                continue;
//            }
//            path.push(p);
//            visited[p.x][p.y] = 1;
//            if (p.equals(end)) {
//                break;  //Reached the last element
//            }
//            int oSize = stack.size();
//            List<Point> lst = findWays(a, p);
//            for (Point pp : lst) {
//                if (!isVisited(a, visited, pp)) {
//                    stack.push(pp);
//                }
//            }
//            int fSize = stack.size();
//            if (fSize <= oSize) {
//                path.pop();
//            }
//            if (fSize - oSize > 1) {
//                count++;
//            }
//        }
//        return new Result(path, count);
//    }

    static Stack<Point> finalPath = new Stack<Point>();
//    static int finalCount = 0;
//    static boolean found = false;

    public static boolean findPath(int[][] a, Point start, Point end, int[][] visited) {
//        List<Point> lstPath = new ArrayList<Point>();
        List<Point> lst = findWays(a, start);
        boolean found = false;
        visited[start.x][start.y] = 1;
        for (Point pp : lst) {
            if (found) {
                break;
            }
            if (pp.x == end.x && pp.y == end.y) {
                visited[pp.x][pp.y] = 1;
                finalPath.push(pp);
                found = true;
                break;
            }
            if (!isVisited(a, visited, pp)) {
                found = findPath(a, pp, end, visited);
            }
        }
        if (found) {
            finalPath.add(start);
//            if (lst.size() > 1) {
//                finalCount++;
//            }
        }
        return found;
    }
//    public static boolean traversePath(int[][] a, Point start, Point end, int[][] visited) {
//        if (visited[start.x][start.y] != 1) {
//            visited[start.x][start.y] = 1;
//            finalPath.push(start);
//            List<Point> lst = findWays(a, start);
//            boolean found = false;
//            for (Point pp : lst) {
////                if (found) {
////                    break;
////                }
//                if (pp.x == end.x && pp.y == end.y) {
//                    finalPath.push(pp);
//                    found = true;
//                    break;
//                }
//                if (!isVisited(a, visited, pp)) {
//                    found = traversePath(a, pp, end, visited);
//                    while (!found) {
//                        Point p = finalPath.pop();
//                        if (!p.equals(start)) {
//                            finalPath.push(p);
//                            break;
//                        }
//                    }
//                }
//            }
////            if (!found && !finalPath.isEmpty()) {
////                finalPath.pop();
////            }
//            return found;
//        } else {
//            return false;
//        }
//    }


    public static boolean isVisited(int[][] a, int[][] visited, Point p) {
        return visited[p.x][p.y] == 1 || a[p.x][p.y] == -1;
    }

    public static List<Point> findWays(int[][] a, Point p) {
        List<Point> lst = new ArrayList<Point>();
        int n = a.length;
        int m = a[0].length;
        int x = p.x;
        int y = p.y;
        //Case 1 - North
        if ((y - 1) >= 0 && a[x][y - 1] == 0) {
            lst.add(new Point(x, y - 1));
        }
        //Case 2 - South
        if ((y + 1) < m && a[x][y + 1] == 0) {
            lst.add(new Point(x, y + 1));
        }
        //Case 3 - East
        if ((x + 1) < n && a[x + 1][y] == 0) {
            lst.add(new Point(x + 1, y));
        }
        //Case 3 - West
        if ((x - 1) >= 0 && a[x - 1][y] == 0) {
            lst.add(new Point(x - 1, y));
        }
        return lst;
    }
}

//...
//*.M

/*
1
2 3
...
*.M
0
*/

/*
1
2 3
XXX
*.M
0
*/

/*

*/