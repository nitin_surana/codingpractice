package hackerrank.bookingdotcom_ioasm9pm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class pcsm0qil8844 {

    static class Hotel {
        Hotel(int id, int value) {
            this.id = id;
            this.value = value;
        }

        public int id;
        public int value;
    }

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<String> setMentions = new TreeSet<String>(Arrays.asList(cleanup(br.readLine()).split(" ")));
        int n = Integer.parseInt(br.readLine());

        Map<Integer, Integer> hotelRanking = new HashMap<Integer, Integer>();//new Comparator<Integer>() {
        ArrayList<Hotel> list = new ArrayList<Hotel>();

        while (n-- > 0) {
            int hotelId = Integer.parseInt(br.readLine());
            int count = 0;
            if (hotelRanking.get(hotelId) != null) {
                count = hotelRanking.get(hotelId);
            }
            String[] arr = cleanup(br.readLine()).split(" ");
            Arrays.sort(arr);
            count += countMentions(setMentions, arr);
            hotelRanking.put(hotelId, count);
        }
        for (Map.Entry<Integer, Integer> entry : hotelRanking.entrySet()) {
            list.add(new Hotel(entry.getKey(), entry.getValue()));
        }
        Collections.sort(list, new Comparator<Hotel>() {
            public int compare(Hotel o1, Hotel o2) {
                if (o1.value == o2.value) {
                    return o1.id - o2.id;
                }
                return o2.value - o1.value;
            }
        });
        for (Hotel h : list) {
//            System.out.println(h.id + "  " + h.value);
            System.out.print(h.id + " ");
        }
        System.out.println();
    }

    public static String cleanup(String str) {
        return str.toLowerCase().replace(",", " ").replace(".", " ").trim();
    }

    public static int countMentions(TreeSet<String> mentions, String[] str) {
        int count = 0;
        for (String x : mentions) {
            int index = -1;
            index = Arrays.binarySearch(str, x);
            int i = 1;
            if (index >= 0) {
                count++;
                while (index + i < str.length) {
                    if (str[index + i].equals(x)) {
                        count++;
                    } else {
                        break;
                    }
                    i++;
                }
                i = 1;
                while (index - i > -1) {
                    if (str[index - i].equals(x)) {
                        count++;
                    } else {
                        break;
                    }
                    i++;
                }
            }
        }
        return count;
    }
}
