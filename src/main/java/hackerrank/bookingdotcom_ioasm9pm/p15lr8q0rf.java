package hackerrank.bookingdotcom_ioasm9pm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class p15lr8q0rf {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int sum = Integer.parseInt(br.readLine().trim());
        int n = Integer.parseInt(br.readLine().trim());
        int[] a = new int[n];
        int index = 0;
        while (n-- > 0) {
            a[index++] = Integer.parseInt(br.readLine().trim());
        }
        if (existsSum(a, sum)) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
    }

    public static boolean existsSum(int[] a, int sum) {
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < a.length; i++) {
            int diff = sum - a[i];
            if (set.contains(a[i])) {
                return true;
            } else {
                set.add(diff);      // num = 10, a[i]=6, set contains 4, if a 4 shows up, we're done!
            }
        }
        return false;
    }
}
