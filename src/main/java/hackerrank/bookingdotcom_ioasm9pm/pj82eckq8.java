package hackerrank.bookingdotcom_ioasm9pm;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class pj82eckq8 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str[] = br.readLine().split(" ");
        int a[] = new int[str.length];
        for (int i = 0; i < a.length; i++) {
            a[i] = Integer.parseInt(str[i]);
        }
        if (a.length >= 1) {
            StringBuffer buffer = new StringBuffer();
            buffer.append(a[0]);
            for (int i = 1; i < a.length; i++) {
                int x = a[i] - a[i - 1];
                if (x <= 127 && x >= -127) {
                    buffer.append(" " + x);
                } else {
                    buffer.append(" -128 " + x);
                }
            }
            System.out.println(buffer.toString());
        }
    }
}
