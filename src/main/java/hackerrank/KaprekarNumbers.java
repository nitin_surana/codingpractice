package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class KaprekarNumbers {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int p = Integer.parseInt(br.readLine());
        int q = Integer.parseInt(br.readLine());
        StringBuffer buffer = new StringBuffer();
        int count = 0;
        for (long i = p; i <= q; i++) {
            int originalDigits = new String(i + "").length();
            long sq = i * i;
            String str = sq + "";
            String rightPiece = str.substring(str.length() - originalDigits, str.length());
            String leftPiece = "0";
            if (str.length() > 1) {
                leftPiece = str.substring(0, str.lastIndexOf(rightPiece));
            }
            long l1 = Long.parseLong(rightPiece);
            long l2 = Long.parseLong(leftPiece);
            if (l1 + l2 == i) {
                count++;
                buffer.append(i + " ");
            }
        }
        if (count == 0) {
            System.out.println("INVALID RANGE");
        } else {
            System.out.println(buffer.toString().trim());
        }
    }
}
