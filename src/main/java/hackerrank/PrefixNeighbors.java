package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class PrefixNeighbors {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, br.readLine().split(" "));
        Collections.sort(list);
        int value = 0;
        value += strValue(list.get(n - 1));
//        System.out.println(list.get(n - 1));
        for (int i = n - 2; i >= 0; i--) {
            if (!list.get(i + 1).contains(list.get(i))) {
//                System.out.println(list.get(i));
                value += strValue(list.get(i));
            } else if (i - 1 >= 0) {
                i--;
//                System.out.println(list.get(i));
                value += strValue(list.get(i));
            }
        }
        System.out.println(value);
    }

    public static int strValue(String s) {
        int v = 0;
        for (int x : s.toCharArray()) {
            v += x;
        }
        return v;
    }
}
