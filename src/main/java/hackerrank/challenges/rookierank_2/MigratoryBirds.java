package hackerrank.challenges.rookierank_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MigratoryBirds {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] str = br.readLine().split(" ");
        int a[] = new int[]{0, 0, 0, 0, 0};
        int max = 0;
        for (int i = 0; i < n; i++) {
            int x = Integer.parseInt(str[i]);
            x--;
            a[x]++;
            if (a[x] > max) {
                max = a[x];
            }
        }
        for (int i = 0; i < 5; i++) {
            if (max == a[i]) {
                System.out.println(i + 1);
                break;
            }
        }
    }
}
