package hackerrank.challenges.rookierank_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class KnightlOnChessboard {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        int originalBoard[][] = new int[n][n];
        int result[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                originalBoard[i][j] = -1;
                result[i][j] = -1;
            }
        }

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                if (result[j][i] != -1) {
                    result[i][j] = result[j][i];
                    continue;
                }
                int board[][] = cloneArray(originalBoard);

                int length = 1;
                Move end = new Move(n - 1, n - 1);
                List<Move> queue = new ArrayList<Move>();
                board[0][0] = 0;
                queue.add(new Move(0, 0));
                while (!queue.isEmpty()) {
                    List<Move> nextQueue = new ArrayList<Move>();
                    for (Move m : queue) {
                        List<Move> moves = findMoves(board, m, i, j, length);
                        boolean found = findEndMove(moves, end);
                        if (found) {
                            result[i][j] = length;
                            break;
                        }
                        nextQueue.addAll(moves);
                    }
                    queue = nextQueue;
                    length++;
                }
            }
        }
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] cloneArray(int[][] src) {
        int length = src.length;
        int[][] target = new int[length][src[0].length];
        for (int i = 0; i < length; i++) {
            System.arraycopy(src[i], 0, target[i], 0, src[i].length);
        }
        return target;
    }

    public static boolean findEndMove(List<Move> moves, Move end) {
        for (Move m : moves) {
//            System.out.println(m.x + "   " + m.y);
            if (m.equals(end)) {
                return true;
            }
        }
        return false;
    }

    //Eight possible moves
    public static List<Move> findMoves(int[][] board, Move start, int a, int b, int scoreToGive) {
        List<Move> moves = new ArrayList<Move>();
        //Move 1
        int newX = start.x + a;
        int newY = start.y - b;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 2
        newX = start.x - a;
        newY = start.y - b;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 3
        newX = start.x - a;
        newY = start.y + b;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 4
        newX = start.x + a;
        newY = start.y + b;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 5
        newX = start.x + b;
        newY = start.y - a;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
        }
        //Move 6
        newX = start.x - b;
        newY = start.y - a;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 7
        newX = start.x - b;
        newY = start.y + a;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        //Move 8
        newX = start.x + b;
        newY = start.y + a;
        if (newX >= 0 && newY >= 0 && newX < board.length && newY < board[0].length && board[newX][newY] == -1) {
            moves.add(new Move(newX, newY));
            board[newX][newY] = scoreToGive;
        }
        return moves;
    }

    static class Move {
        int x, y;

        Move(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Move) {
                Move m = (Move) obj;
                return (m.x == this.x && m.y == this.y);
            }
            return true;
        }
    }
}
