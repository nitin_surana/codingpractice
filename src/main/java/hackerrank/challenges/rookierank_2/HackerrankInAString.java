package hackerrank.challenges.rookierank_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HackerrankInAString {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String original = "hackerrank";

        int q = Integer.parseInt(br.readLine());

        while (q-- > 0) {
            String str = br.readLine().toLowerCase().trim();
            int indexFrom = 0;
            int charIndex = 0;
            boolean found = true;
            int lastIndex = -1;

            while (charIndex < original.length() && indexFrom < str.length()) {
                indexFrom = str.indexOf(original.charAt(charIndex++), indexFrom);
                if (indexFrom <= lastIndex) {
                    found = false;
                    break;
                }
                lastIndex = indexFrom;
                indexFrom++;
            }
            if (found && charIndex == original.length()) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
