package hackerrank.challenges.rookierank_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class PrefixNeighbors {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] str = br.readLine().split(" ");//new String[]{"A", "B", "AE"};
        String[] bv = new String[str.length];
        Arrays.sort(str);       //Alphabetical sorting
        Set<String> visited = new HashSet<String>();
        System.arraycopy(str, 0, bv, 0, str.length);
        Arrays.sort(bv, new Comparator<String>() {      //reverse order of benefit-value (highest benefit value first)
            public int compare(String o1, String o2) {
                return calcBV(o2) - calcBV(o1);
            }
        });

        int maxBV = 0;
        List<String> resultSet = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            String s = bv[i];
            if (visited.contains(s)) {
                continue;
            }
            if (!visited.contains(s)) {
                maxBV += calcBV(s);
                resultSet.add(s);
            }
            visited.add(s);

            //Find it in str and then find the one before it, if that's a prefix, mark the prefix visited
            int index = Arrays.binarySearch(str, s);
            while (index > 0) {
                if (s.contains(str[index - 1])) {
                    visited.add(str[index - 1]);
                    break;
                }
                index--;
            }
        }
        System.out.println(maxBV);
        for (String x : resultSet) {
            System.out.println(x);
        }
    }

    public static int calcBV(String str) {
        int value = 0;
        for (int c : str.toCharArray()) {
            value += c;
        }
        return value;
    }
}
