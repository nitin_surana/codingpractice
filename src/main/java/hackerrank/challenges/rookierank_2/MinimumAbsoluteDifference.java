package hackerrank.challenges.rookierank_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MinimumAbsoluteDifference {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String str[] = br.readLine().split(" ");
        long[] a = new long[n];
        for (int i = 0; i < n; i++) {
            a[i] = Long.parseLong(str[i]);
        }
        Arrays.sort(a);
        long min = Long.MAX_VALUE;
        for (int i = 0; i < n - 1; i++) {
            long diff = Math.abs(a[i] - a[i + 1]);
            if (diff < min) {
                min = diff;
            }
        }
        System.out.println(min);
    }
}
