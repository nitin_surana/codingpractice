package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class CutTheTree {

    static class Node {
        int data;
        long sum;
        Node parent;
        List<Node> nodes = new ArrayList<Node>();

        public Node(int data) {
            this.data = data;
        }

        public void connectToNode(Node v) {
            nodes.add(v);
        }

        public List<Node> getAllNodes() {
            return nodes;
        }
    }

    static class Edge {
        Node start;
        Node end;

        Edge(Node start, Node end) {
            this.start = start;
            this.end = end;
        }
    }

    static LinkedHashMap<Node, List<Node>> mapNodes = new LinkedHashMap<Node, List<Node>>();

    public static void main(String[] args) throws Exception {
//        System.out.println(System.currentTimeMillis());
//        long startTime = System.currentTimeMillis() / 1000;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] str = br.readLine().split(" ");
        Node a[] = new Node[n];
        for (int i = 0; i < n; i++) {
            a[i] = new Node(Integer.parseInt(str[i]));
        }

        List<Edge> edges = new ArrayList<Edge>();
        for (int i = 0; i < n - 1; i++) {
            str = br.readLine().split(" ");
            int u = Integer.parseInt(str[0]);
            int v = Integer.parseInt(str[1]);
            u--;
            v--;
            a[u].connectToNode(a[v]);
            a[v].connectToNode(a[u]);
            edges.add(new Edge(a[u], a[v]));
        }

        Node rootNode = a[0];
        List<Node> visited = new ArrayList<Node>();
        visited.add(rootNode);
        doDFS(rootNode, visited);    //  a, 0, visited);

//        for (Node nn : a) {
//            System.out.println(nn.data + "  " + nn.sum);
//        }

        long totalSum = rootNode.sum;
        long min = Long.MAX_VALUE;
        String xx = "";
        for (Edge edge : edges) {
            long diff = 0;
            Node x = edge.start;
            Node y = edge.end;
            if (x.parent == y) {
                diff = Math.abs(totalSum - x.sum - x.sum);
            } else {
                diff = Math.abs(totalSum - y.sum - y.sum);
            }
            if (min > diff) {
                min = diff;
                xx = x.sum + "  " + y.sum + "     " + totalSum;
            }
        }
        System.out.println(min);
//        System.out.println("Min : " + min);
//        System.out.println(xx);
//        System.out.println(System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() / 1000 - startTime);
//        for (Node nn : a) {
//            if (nn.parent == null) {
//                System.out.println(nn.data + "    " + nn.sum);
//            }
//        }
    }

//    public static void doDFS(Node[] arr, Node start, List<Node> visited) {
//        Set<Node> leaves = new HashSet<Node>();
//        //Find all leaves
//        for (Node n : arr) {
//            n.sum = n.data;
//            if (n.getAllNodes().size() == 1 && n != start) {
//                leaves.add(n);
//                visited.add(n);
//                n.parent = n.getAllNodes().get(0);
//            }
//        }
//        //Backtrack back from leaves to the root and propagate sum
//        while (!leaves.isEmpty()) {
//            Set<Node> nodeList = new HashSet<Node>();
//            for (Node n : leaves) {
//                if (n.parent != null && !visited.contains(n.parent)) {
//                    int tempVisited = 0;
//                    long tempSum = 0;
//                    int tempChildCount = 0;
//                    Node possibleParent = null;
//                    int tempParentCount = 0;
//                    for (Node nn : n.parent.getAllNodes()) {
//                        if (nn.parent == n.parent) {
//                            if (visited.contains(nn)) {
//                                tempVisited++;
//                            }
//                            tempSum += nn.sum;
//                            tempChildCount++;
//                        } else {
//                            possibleParent = nn;
//                            tempParentCount++;
//                        }
//                    }
//                    if (tempVisited == tempChildCount) {
//                        if ((n.parent == start && tempParentCount == 0) || tempParentCount == 1) {
//                            n.parent.sum += tempSum;
//                            nodeList.add(n.parent);
//                            visited.add(n.parent);
//                        }
//                    }
//                    if (n.parent != start && tempChildCount + 1 == n.parent.getAllNodes().size()) {
//                        n.parent.parent = possibleParent;
//                    }
//                }
//            }
//            leaves = nodeList;
//        }
//    }

    //Iterative solution
//    public static void doDFS(Node start, List<Node> visited) {
//        Stack<Node> stack = new Stack<Node>();
//        stack.push(start);
//        Set<Node> lst = new HashSet<Node>();
////        System.out.println("Before DFS - " + System.currentTimeMillis());
//        while (!stack.isEmpty()) {
//            Node n = stack.pop();
//            n.sum = n.data;
//
//            List<Node> nodes = n.getAllNodes();
//            mapNodes.put(n, nodes);
//
//            int count = 0;
//            for (Node node : nodes) {
//                if (!visited.contains(node)) {
//                    visited.add(node);
//                    node.parent = n;
//                    stack.push(node);
//                    count++;
//                }
//            }
//            if (count == 0) {
//                lst.add(n);
//            }
//        }
////        System.out.println("After DFS - " + System.currentTimeMillis());
//
//        visited = new ArrayList<Node>(lst);
////        for (Node nn : lst) {
////            visited.add(nn);
////        }
////        System.out.println("Before somewhat BFS - " + System.currentTimeMillis());
//        while (!lst.isEmpty()) {
//            Set<Node> nodeList = new HashSet<Node>();
//            for (Node n : lst) {
//                if (n.parent != null && !visited.contains(n.parent)) {
//                    int tempVisited = 0;
//                    long tempSum = 0;
//                    int tempChildCount = 0;
//                    for (Node nn : n.parent.getAllNodes()) {
//                        if (nn.parent == n.parent) {
//                            if (visited.contains(nn)) {
//                                tempVisited++;
//                            }
//                            tempSum += nn.sum;
//                            tempChildCount++;
//                        }
//                    }
//                    if (tempVisited == tempChildCount) {
//                        n.parent.sum += tempSum;
//                        nodeList.add(n.parent);
//                        visited.add(n.parent);
//                    }
//                }
//            }
//            lst = nodeList;
//        }
////        System.out.println("After somewhat BFS - " + System.currentTimeMillis());
//    }

    //Recursive
    public static long doDFS(Node start, List<Node> visited) {
        List<Node> nodes = start.getAllNodes();
        long sum = start.data;
        for (int i = 0; i < nodes.size(); i++) {
            Node nn = nodes.get(i);
            if (!visited.contains(nn)) {
                System.out.println("Visiting : " + nn.data);
                nn.parent = start;
                visited.add(nn);
                sum += doDFS(nn, visited);
            }
        }
        start.sum = sum;
        return sum;
    }
}

/*
5
100 200 300 400 500
1 2
1 3
3 4
3 5
 */

/*
5
100 200 300 400 500
1 2
1 3
2 4
2 5
 */

/*
7
100 200 300 400 500 600 700
1 2
1 3
2 4
2 5
3 6
3 7
 */

/*
3
200 2000 100
1 2
2 3
 */

/*
5
200 2000 100 300 600
1 2
2 3
2 4
4 5
 */
