package hackerrank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AlmostSorted {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        int a[] = new int[n];
        int b[] = new int[n];
        String str[] = br.readLine().split(" ");
        for (int i = 0; i < n; i++) {
            a[i] = Integer.parseInt(str[i]);
        }
        System.arraycopy(a, 0, b, 0, n);
        Arrays.sort(b);

        int misMatch = 0;
        int l = -1;
        int r = n;
        for (int i = 0; i < n; i++) {
            if (a[i] != b[i]) {
                if (l == -1) {
                    l = i;
                }
                misMatch++;
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            if (a[i] != b[i]) {
                r = i;
                break;
            }
        }


        if (misMatch == 0) {        //already sorted
            System.out.println("yes");
        } else if (misMatch == 2) {
            System.out.println("yes");
            System.out.println("swap " + (l + 1) + " " + (r + 1));
        } else if (misMatch > 2) {
            int original_r = r;
            int original_l = l;
            while (l <= original_r && a[l] == b[r]) {
                l++;
                r--;
            }
            if (l == original_r + 1) {
                System.out.println("yes");
                System.out.println("reverse " + (original_l + 1) + " " + (original_r + 1));
            } else {
                System.out.println("no");
            }
        }
    }
}
