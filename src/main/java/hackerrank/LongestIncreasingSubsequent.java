package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class LongestIncreasingSubsequent {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        int a[] = new int[n];
        int s[] = new int[n];
        int lindex = 0;
        int lis = 1;
        for (int i = 0; i < n; i++) {
            a[i] = Integer.parseInt(br.readLine());
        }
        s[lindex] = a[lindex];
        for (int i = 1; i < n; i++) {
            int x = ceil(s, 0, lindex + 1, a[i]);
            if (x == -1) {
                lindex++;
                lis++;
                s[lindex] = a[i];
            } else {
                s[x] = a[i];
            }
        }
        System.out.println(lis);
    }

    public static void main2(String[] args) {
        int a[] = new int[]{2, 4, 6, 8, 10};
        System.out.println(ceil(a, 0, a.length, 1));
        System.out.println(ceil(a, 0, a.length, 2));
        System.out.println(ceil(a, 0, a.length, 3));
        System.out.println(ceil(a, 0, a.length, 4));
        System.out.println(ceil(a, 0, a.length, 5));
        System.out.println(ceil(a, 0, a.length, 6));
        System.out.println(ceil(a, 0, a.length, 7));
        System.out.println(ceil(a, 0, a.length, 8));
        System.out.println(ceil(a, 0, a.length, 9));
        System.out.println(ceil(a, 0, a.length, 10));
        System.out.println(ceil(a, 0, a.length, 11));

        System.out.println();
        a = new int[]{2};
        System.out.println(ceil(a, 0, a.length, 1));
        System.out.println(ceil(a, 0, a.length, 2));
        System.out.println(ceil(a, 0, a.length, 11));

        System.out.println();
        a = new int[]{2, 7};
        System.out.println(ceil(a, 0, a.length, 4));

        System.out.println();
        a = new int[]{0, 22, 33};
        System.out.println(ceil(a, 0, a.length, 24));
    }

    public static int ceil(int[] a, int start, int end, int f) {
        if (start >= end) {
            return -1;
        }
        int m = (end - start) / 2;
        m += start;
        if (a[m] == f) {
            return m;
        } else if (a[m] < f) {
            return ceil(a, m + 1, end, f);
        } else if (a[m] > f) {
            if ((end - start) == 1 || a[m - 1] < f) {
                return m;
            } else {
                return ceil(a, start, m, f);
            }
        }
        return -1;
    }
}
