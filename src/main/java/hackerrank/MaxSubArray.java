package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MaxSubArray {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine().trim());
        while (t-- > 0) {
            int n = Integer.parseInt(br.readLine().trim());
            String[] str = br.readLine().trim().split(" ");
            long[] a = new long[n];
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(str[i]);
            }
            long max = Long.MIN_VALUE;
            long sum = 0;
            long nc_sum = 0;
            long maxValue = Long.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                if (a[i] > maxValue) {      //All negatives
                    maxValue = a[i];
                }
                if (a[i] > 0) {
                    nc_sum += a[i];
                }
                sum += a[i];
                if (sum > max && sum != 0) {
                    max = sum;
                }
                if (sum < 0) {
                    sum = 0;
                }
            }
            if (max == Long.MIN_VALUE) {        //all negatives, all 0s
                max = 0;
            }
            if (nc_sum == 0) {                  //all negatives, all 0s
                nc_sum = maxValue;
            }
            System.out.println(max + " " + nc_sum);
        }
    }
}
