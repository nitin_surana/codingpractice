package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Encryption {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        int rows = (int) Math.floor(Math.sqrt(str.length()));
        int cols = (int) Math.ceil(Math.sqrt(str.length()));
        if (rows * cols < str.length()) {
            rows++;
        }
        char a[][] = new char[rows][cols];
        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (index < str.length()) {
                    a[i][j] = str.charAt(index++);
                } else {
                    a[i][j] = ' ';
                }
            }
        }

        StringBuffer output = new StringBuffer();
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows && a[j][i] != ' '; j++) {
                output.append(a[j][i]);
            }
            output.append(" ");
        }
        System.out.println(output.toString().trim());
    }
}
