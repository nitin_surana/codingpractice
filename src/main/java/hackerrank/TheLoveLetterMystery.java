package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TheLoveLetterMystery {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            char[] a = br.readLine().toCharArray();
            int n = palindromeOperations(a);
            System.out.println(n);
        }
    }

    public static int palindromeOperations(char[] a) {
        int oper = 0;
        int n = a.length;
        int mid = n / 2;        //  5/2=2       4/2=2 (should be 1)
        if (n % 2 == 0) {
            mid = (n + 1) / 2;
        }
        for (int i = 0; i < mid; i++) {
            if (a[i] != a[n - 1 - i]) {
                int m = Math.max(a[i], a[n - 1 - i]) - Math.min(a[i], a[n - 1 - i]);
                oper += m;
            }
        }
        return oper;
    }
}
