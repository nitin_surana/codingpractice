package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UnboundedKnapsack {

    static int remainder = Integer.MAX_VALUE;

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            remainder = Integer.MAX_VALUE;
            String str[] = br.readLine().split(" ");
            int n = Integer.parseInt(str[0]);
            int sum = Integer.parseInt(str[1]);
            int a[] = new int[n];
            str = br.readLine().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(str[i]);
            }
            minRemainer(a, 0, 0, sum);
            if (remainder == Integer.MAX_VALUE) {
                System.out.println(0);
            } else {
                System.out.println(sum - remainder);
            }
        }
    }

    public static void minRemainer(int[] a, int sum, int index, int expectedSum) {
        int r = Integer.MAX_VALUE;
        if (sum <= expectedSum && sum != 0) {
            r = expectedSum % sum;
            if (r < remainder) {
                remainder = r;
            }
        }

        if (index >= a.length) {
            return;
        }

        minRemainer(a, sum, index + 1, expectedSum);
        if (sum + a[index] <= expectedSum) {
            minRemainer(a, sum + a[index], index + 1, expectedSum);
        }
    }
}
