package hackerrank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MorganAndAString {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while (t-- > 0) {
            String first = br.readLine();
            String second = br.readLine();

            int i = 0, j = 0;
            StringBuffer result = new StringBuffer();
            int tempI = 0;
            int tempJ = 0;
            while (i < first.length() && j < second.length()) {
                if (first.charAt(i) < second.charAt(j)) {
                    result.append(first.charAt(i++));
                } else if (first.charAt(i) > second.charAt(j)) {
                    result.append(second.charAt(j++));
                } else {
                    if (i > tempI || j > tempJ) {
                        tempI = i;
                        tempJ = j;
                    }
                    while (tempI < first.length() && tempJ < second.length()) {
                        if (first.charAt(tempI) == second.charAt(tempJ)) {
                            tempI++;
                            tempJ++;
                        } else {
                            if (first.charAt(tempI) < second.charAt(tempJ)) {
                                result.append(first.charAt(i++));
                            } else {
                                result.append(second.charAt(j++));
                            }
                            break;
                        }
                    }
                    if (tempI == first.length() && tempJ == second.length()) {
                        result.append(first.charAt(i++));
                    } else if (tempI == first.length() && second.charAt(tempJ) < second.charAt(tempJ - 1)) {
                        result.append(second.charAt(j++));
                    } else if (tempI == first.length() && second.charAt(tempJ) > second.charAt(tempJ - 1)) {
                        result.append(first.charAt(i++));
                    } else if (tempJ == second.length() && first.charAt(tempI) < first.charAt(tempI - 1)) {
                        result.append(first.charAt(i++));
                    } else if (tempJ == second.length() && first.charAt(tempI) > first.charAt(tempI - 1)) {
                        result.append(second.charAt(j++));
                    }
                }
            }
            while (i < first.length()) {
                result.append(first.charAt(i++));
            }
            while (j < second.length()) {
                result.append(second.charAt(j++));
            }
            System.out.println(result);
        }
    }
}
