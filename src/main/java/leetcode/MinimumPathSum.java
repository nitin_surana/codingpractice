package leetcode;

public class MinimumPathSum {

    static public int minPathSum(int[][] grid) {
        int c[][] = new int[grid.length][grid[0].length];
        c[0][0] = grid[0][0];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (i != 0 || j != 0) {
                    if (i - 1 < 0) {
                        c[i][j] = c[i][j - 1];
                    } else if (j - 1 < 0) {
                        c[i][j] = c[i - 1][j];
                    } else {
                        c[i][j] = Math.min(c[i - 1][j], c[i][j - 1]);
                    }
                    c[i][j] += grid[i][j];
                }
            }
        }
        return c[grid.length - 1][grid[0].length - 1];
    }

    public static void main(String[] args) {
        int grid[][] = new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        System.out.println(minPathSum(grid));
    }
}
