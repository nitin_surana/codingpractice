package leetcode;

public class ClimbingStairs {

    static public int climbStairs(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (n == 2) {
            return 2;
        } else {
            int a[] = new int[n + 1];
            a[0] = 0;
            a[1] = 1;
            a[2] = 2;
            for (int i = 3; i <= n; i++) {
                a[i] = a[i - 1] + a[i - 2];
            }
            return a[n];
        }
    }


    public static void main(String[] args) {
        System.out.println(climbStairs(1) == 1);
        System.out.println(climbStairs(2) == 2);
        System.out.println(climbStairs(3) == 3);
    }
}
