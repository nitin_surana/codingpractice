package leetcode;

public class CountNumbersWithUniqueDigits {

    public static void main(String[] args) {
        System.out.println(countNumbersWithUniqueDigits(1) == 10);
        System.out.println(countNumbersWithUniqueDigits(2) == 91);
        System.out.println(countNumbersWithUniqueDigits(3) == 739);
    }

    static public int countNumbersWithUniqueDigits(int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            count += permute(9, i);
        }
        count *= 9;
        count += 1;
        return count;
    }

    static public int permute(int n, int r) {
        int mul = 1;
        int temp = 0;
        while (temp < r) {
            mul *= (n - temp++);
        }
        return mul;
    }
}
