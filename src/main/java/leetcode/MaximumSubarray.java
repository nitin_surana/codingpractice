package leetcode;

public class MaximumSubarray {
    // Solution 1 - Kandane's Algorithm
//    static public int maxSubArray(int[] nums) {
//        if (nums.length == 0) {
//            return 0;
//        }
//        int maxSum = Integer.MIN_VALUE;
//        int n = nums.length;
//        int sum = 0;
//        for (int i = 0; i < n; i++) {
//            sum += nums[i];
//            if (sum > maxSum) {
//                maxSum = sum;
//            }
//            if (sum < 0) {
//                sum = 0;
//            }
//        }
//        return maxSum;
//    }

    static public int maxSubArray(int[] nums) {
        return maxSubArrayRecur(nums, 0, nums.length);
    }

    static public int maxSubArrayRecur(int[] nums, int start, int end) {
        if (start > end) {
            return 0;
        } else if (start + 1 == end) {
            return nums[start];
        }
        int m = (start + end) / 2;
        int maxLeftSum = Integer.MIN_VALUE;
        int sumLeft = 0;
        for (int i = start; i < m; i++) {
            sumLeft += nums[i];
            if (sumLeft > maxLeftSum) {
                maxLeftSum = sumLeft;
            }
        }
        int maxRightSum = Integer.MIN_VALUE;
        int sumRight = 0;
        for (int i = m + 1; i < end; i++) {
            sumRight += nums[i];
            if (sumRight > maxRightSum) {
                maxRightSum = sumRight;
            }
        }
        int totalSum = maxLeftSum + maxRightSum;
        int leftSubArraySum = maxSubArrayRecur(nums, start, m);
        int rightSubArraySum = maxSubArrayRecur(nums, m , end);
        return m + Math.max(totalSum, Math.max(leftSubArraySum, rightSubArraySum));
    }


    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5};
        System.out.println(maxSubArray(a) == 15);
        a = new int[]{0, 0, 0, 0};
        System.out.println(maxSubArray(a) == 0);
        a = new int[]{-2, -4, -6, -5};
        System.out.println(maxSubArray(a) == -2);
        a = new int[]{-2, -4, -6, 5};
        System.out.println(maxSubArray(a) == 5);
    }
}
