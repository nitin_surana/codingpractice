package leetcode;

public class HouseRobber {

    static public int rob(int[] a) {
        int max[] = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            if (i - 2 >= 0) {
                max[i] = Math.max(max[i - 1], max[i - 2] + a[i]);
            } else if (i - 1 >= 0) {
                max[i] = Math.max(max[i - 1], a[i]);
            } else {
                max[i] = a[i];
            }
        }
        int mm = 0;
        for (int x : max) {
            if (x > mm) {
                mm = x;
            }
        }
        return mm;
    }

    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 100};
        System.out.println(rob(a) == 120);
        a = new int[]{2, 1, 1, 2};
        System.out.println(rob(a) == 4);
        a = new int[]{2, 1, 3, 2};
        System.out.println(rob(a) == 5);
        a = new int[]{2, 1, 3, 5};
        System.out.println(rob(a) == 7);
    }
}
