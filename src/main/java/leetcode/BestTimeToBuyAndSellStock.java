package leetcode;

public class BestTimeToBuyAndSellStock {
    //    https://leetcode.com/problems/best-time-to-buy-and-sell-stock/?tab=Description
    static public int maxProfit(int[] prices) {
        if (prices.length > 1) {
            int max = prices[0];
            int min = prices[0];
            int profit = 0;
            for (int i = 1; i < prices.length; i++) {
                if (prices[i] > max) {
                    max = prices[i];
                    int temp = max - min;
                    if (temp > profit) {
                        profit = temp;
                    }
                } else if (prices[i] < min) {
                    min = prices[i];
                    max = prices[i];
                }
            }
            return profit;
        }
        return 0;
    }

    public static void main(String[] args) {
        int a[] = new int[]{1, 2, 3, 45, 6, 8, 7, 5, 10, 16};
        System.out.println(maxProfit(a) == 44);
        a = new int[]{1, 2, 3, 4, 5};
        System.out.println(maxProfit(a) == 4);
        a = new int[]{};
        System.out.println(maxProfit(a) == 0);
        a = new int[]{1};
        System.out.println(maxProfit(a) == 0);
        a = new int[]{1, 2};
        System.out.println(maxProfit(a) == 1);
        a = new int[]{3, 2, 1};
        System.out.println(maxProfit(a) == 0);
        a = new int[]{3, 2, 6, 0, 3};
        System.out.println(maxProfit(a) == 4);
    }
}
