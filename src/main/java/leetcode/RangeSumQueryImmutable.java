package leetcode;

public class RangeSumQueryImmutable {
    static class NumArray {
        int[] a;
        int[] sum;

        public NumArray(int[] nums) {
            a = nums;
            if (nums != null && nums.length > 0) {
                sum = new int[nums.length];
                sum[0] = nums[0];
                for (int i = 1; i < nums.length; i++) {
                    sum[i] = sum[i - 1] + a[i];
                }
            }
        }

        public int sumRange(int i, int j) {
            if (sum != null && sum.length > 0 && i >= 0 && j < sum.length) {
                int s = 0;
                if (i > 0) {
                    s = sum[i - 1];
                }
                return sum[j] - s;
            }
            return 0;
        }
    }

    public static void main(String[] args) {
        int a[] = new int[]{1, 2, 3, 4, 5, 6, -7};
        NumArray obj = new NumArray(a);
        System.out.println(obj.sumRange(0, 1) == 3);
        System.out.println(obj.sumRange(0, 0) == 1);
        System.out.println(obj.sumRange(0, 4) == 15);
        System.out.println(obj.sumRange(0, 5) == 21);
        System.out.println(obj.sumRange(0, 6) == 14);
        System.out.println(obj.sumRange(1, 6) == 13);
        System.out.println(obj.sumRange(2, 6) == 11);
        System.out.println(obj.sumRange(3, 6) == 8);

        obj = new NumArray(null);
        System.out.println(obj.sumRange(0, 1));
        System.out.println(obj.sumRange(0, 5));

    }
}
