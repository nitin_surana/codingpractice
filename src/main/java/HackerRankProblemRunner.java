import hackerrank.MorganAndAString;
import hackerrank.university_codesprint_2.GameOfTwoStacks;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class HackerRankProblemRunner {

    public static void main(String[] args) {
        try {
            PrintStream consolePrintStream = System.out;

            System.setIn(new FileInputStream(new File("input.txt")));
            System.setOut(new PrintStream(new File("output.txt")));
            MorganAndAString.main(new String[]{});

            List<String> expectedOutput = (Files.readAllLines(Paths.get("expected_output.txt")));
            List<String> actualOutput = (Files.readAllLines(Paths.get("output.txt")));

            System.setOut(consolePrintStream);
            for (int i = 0; i < expectedOutput.size(); i++) {
                if (expectedOutput.get(i).trim().equals(actualOutput.get(i).trim())) {
                    System.out.println(true);
                } else {
                    System.out.println();
                    System.out.println("Case # " + (i + 1));
                    System.out.println("Expected Output");
                    System.out.println(expectedOutput.get(i));
                    System.out.println("Actual Output");
                    System.out.println(actualOutput.get(i));
                    System.out.printf("");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
